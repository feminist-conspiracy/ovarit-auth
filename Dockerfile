# Start by building the Rust code using a rust:latest image
FROM rust:1.77.0 as builder

# Copy the source code into the image
COPY . .

USER root

# Build the Rust code in release mode
RUN cargo build --release

# Use the rust:slim image as the base for the final image
FROM rust:slim

# Copy the compiled binary from the builder image
COPY --from=builder /target/release/ovarit-auth .
COPY static static

# Expose the port that the application runs on
EXPOSE 3000

# Run the compiled binary when the image is started
CMD ["./ovarit-auth"]
