use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::{
    env::{self, temp_dir},
    path::PathBuf,
    sync::Arc,
};
use thiserror::Error;
use url::Url;

#[derive(Clone)]
pub struct PostEndpointsState {
    pub task_management: Arc<dyn TaskManagement + Send + Sync>,
    pub file_management: Arc<dyn FileManagement + Send + Sync>,
}

macro_rules! newtype {
    ($name:ident, $type:ty) => {
        #[derive(Clone, Debug, Deserialize, Serialize)]
        #[serde(transparent)]
        pub struct $name($type);

        impl From<$type> for $name {
            fn from(s: $type) -> Self {
                $name(s)
            }
        }

        impl<'a> From<&'a str> for $name {
            fn from(s: &'a str) -> Self {
                $name(s.to_owned())
            }
        }

        impl std::ops::Deref for $name {
            type Target = $type;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl std::ops::DerefMut for $name {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.0
            }
        }

        impl std::fmt::Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                self.0.fmt(f)
            }
        }
    };
}

#[derive(Clone, Debug, Error)]
pub enum FileManagementError {
    #[error("invalid sid")]
    InvalidSid,
    #[error("invalid title")]
    InvalidTitle,
    #[error("invalid flair")]
    InvalidFlair,
}

newtype!(Sid, String);
newtype!(ImgId, String);
newtype!(UrlId, String);
newtype!(TitleId, String);

#[async_trait]
pub trait TaskManagement {
    async fn enqueue_archive(&self, url: &Url) -> Result<UrlId, TaskManagementError>;
    async fn enqueue_img(&self, url: &Url) -> Result<ImgId, TaskManagementError>;
    async fn enqueue_title(&self, url: &Url) -> Result<TitleId, TaskManagementError>;
}

#[async_trait]
pub trait FileManagement {
    async fn create_upload_url(
        &self,
        user_id: &str,
        sid: &Sid,
        title: &str,
        flair: &str,
    ) -> Result<Url, FileManagementError>;
}

#[derive(Clone, Default)]
pub struct InMemoryTaskManagement {}

#[async_trait]
impl TaskManagement for InMemoryTaskManagement {
    async fn enqueue_archive(&self, _url: &Url) -> Result<UrlId, TaskManagementError> {
        todo!()
    }
    async fn enqueue_img(&self, _url: &Url) -> Result<ImgId, TaskManagementError> {
        todo!()
    }
    async fn enqueue_title(&self, _url: &Url) -> Result<TitleId, TaskManagementError> {
        todo!()
    }
}

#[allow(dead_code)]
#[derive(Clone)]
pub struct OnDiskFileManagement {
    localhost: Url,
    local_path: PathBuf,
}

impl Default for OnDiskFileManagement {
    fn default() -> Self {
        Self {
            localhost: Url::parse(&env::var("BASE_URL").unwrap_or_default()).unwrap_or_else(|_| {
                Url::parse("http://localhost:8000").expect("BASE_URL must be a valid URL")
            }),
            local_path: temp_dir().join("ovarit_upload"),
        }
    }
}

impl OnDiskFileManagement {
    pub fn new(localhost: Url, local_path: PathBuf) -> Self {
        Self {
            localhost,
            local_path,
        }
    }
}

#[async_trait]
impl FileManagement for OnDiskFileManagement {
    async fn create_upload_url(
        &self,
        uid: &str,
        sid: &Sid,
        title: &str,
        flair: &str,
    ) -> Result<Url, FileManagementError> {
        Ok(self
            .localhost
            .join(&format!(
                "upload?uid={uid}&sid={sid}&title={title}&flair={flair}",
            ))
            .unwrap())
    }
}

#[derive(Debug, Error)]
pub enum TaskManagementError {}

pub async fn pre_link(
    _url: &Url,
    _state: &PostEndpointsState,
) -> Result<(ImgId, UrlId, TitleId), TaskManagementError> {
    todo!()
}
