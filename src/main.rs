mod o_auth;
mod pinpoint_email;
mod post_endpoints;
mod postgres_user_store;

use aws_config::BehaviorVersion;
use axum::{
    body::Body,
    extract::{FromRef, FromRequestParts},
    http::{
        header::{self, HeaderName},
        HeaderValue, StatusCode,
    },
    middleware::from_fn_with_state,
    response::Response,
    routing::get,
    Router,
};
use eyre::{eyre, Result};
use hyper::{http::request::Parts, Request};
pub use ovarit_auth;
use ovarit_auth::{
    flask_session::Session,
    post::{InMemoryTaskManagement, OnDiskFileManagement, PostEndpointsState},
    rate_limiter::RateLimiter,
    users::{in_memory, UserError},
    AuthState,
};
use pinpoint_email::PinpointContact;
use postgres_user_store::PostgresUserStore;
use sqlx::PgPool;
use std::{any::Any, env, net::IpAddr, sync::Arc};
use std::{net::SocketAddr, sync::Mutex, time::Duration};
use tower_cookies::CookieManagerLayer;
use tower_http::{
    catch_panic::CatchPanicLayer,
    compression::CompressionLayer,
    propagate_header::PropagateHeaderLayer,
    request_id::{MakeRequestUuid, SetRequestIdLayer},
    services::{ServeDir, ServeFile},
    set_header::SetResponseHeaderLayer,
    trace::{DefaultOnFailure, DefaultOnRequest, DefaultOnResponse, TraceLayer},
    LatencyUnit,
};
use tracing::{error, Level};
use tracing_error::ErrorLayer;
use tracing_log::LogTracer;
use tracing_subscriber::{
    fmt::Layer, prelude::__tracing_subscriber_SubscriberExt, EnvFilter, Registry,
};
use url::Url;

impl FromRequestParts<AppState> for Session {
    type Rejection = (StatusCode, &'static str);

    async fn from_request_parts(
        parts: &mut Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        Self::from_request_parts(parts, &state.auth_state).await
    }
}

#[derive(Clone)]
pub struct AppState {
    auth_state: AuthState,
    post_state: PostEndpointsState,
}

impl FromRef<AppState> for AuthState {
    fn from_ref(app_state: &AppState) -> AuthState {
        app_state.auth_state.clone()
    }
}

impl FromRef<AppState> for PostEndpointsState {
    fn from_ref(app_state: &AppState) -> PostEndpointsState {
        app_state.post_state.clone()
    }
}

#[tracing::instrument()]
pub fn create_pool() -> Result<PgPool> {
    let host = std::env::var("DATABASE_HOST")?;
    let port = std::env::var("DATABASE_PORT")?;
    let password = std::env::var("DATABASE_PASSWORD");
    let user = std::env::var("DATABASE_USER");
    let name = std::env::var("DATABASE_NAME")?;

    let mut database = Url::parse(&format!("postgresql://{}", host))?;
    database.set_path(&name);
    if let Ok(ref user) = user {
        database
            .set_username(user)
            .map_err(|_| eyre!("Username not allowed"))?;
    }
    database
        .set_port(port.parse().ok())
        .map_err(|_| eyre!("Port not allowed"))?;

    tracing::info!("Connecting to database at {database}");

    database
        .set_password(password.ok().as_deref())
        .map_err(|_| eyre!("Password not allowed"))?;

    PgPool::connect_lazy(database.as_str())
        .map_err(|e| eyre!(e).wrap_err("Failed to connect to database"))
}

#[tokio::main]
#[tracing::instrument(skip())]
async fn main() {
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    if env::var("BASE_URL").is_err() {
        // This is as safe as can be as it's the first thing we do within the application.
        unsafe { env::set_var("BASE_URL", format!("http://localhost:{}/", addr.port())) };
    }
    LogTracer::init().unwrap();
    // Create a Registry and add the JSON format layer, environment filter layer, and ErrorLayer to it.
    let subscriber = Registry::default()
        .with(Layer::default().json())
        .with(EnvFilter::from_default_env())
        .with(ErrorLayer::default());

    // Set the global tracing subscriber.
    tracing::subscriber::set_global_default(subscriber).unwrap();

    let shared_config = aws_config::load_defaults(BehaviorVersion::latest()).await;

    // If we are built in release mode, or in dev mode with
    // a database supplied, use Postgres

    let state = if !cfg!(debug_assertions) {
        AppState {
            auth_state: AuthState {
                user_store: {
                    let pool = create_pool();
                    match pool {
                        Ok(pool) => Arc::new(PostgresUserStore { sql_pool: pool }),
                        Err(_) => {
                            error!(
                                "Failed to connect to database, falling back to in-memory store"
                            );
                            Arc::new(in_memory::InMemoryDataStore::default())
                        }
                    }
                },
                user_contact: Arc::new(PinpointContact {
                    client: aws_sdk_pinpoint::Client::new(&shared_config),
                }),
            },
            post_state: PostEndpointsState {
                task_management: Arc::new(InMemoryTaskManagement::default()),
                file_management: Arc::new(OnDiskFileManagement::default()),
            },
        }
    } else {
        AppState {
            auth_state: AuthState {
                user_store: Arc::new(PostgresUserStore {
                    sql_pool: create_pool().unwrap(),
                }),
                user_contact: Arc::new(in_memory::StdOutUserContact),
            },
            post_state: PostEndpointsState {
                task_management: Arc::new(InMemoryTaskManagement::default()),
                file_management: Arc::new(OnDiskFileManagement::default()),
            },
        }
    };

    let app = app(state);

    tracing::info!("Listening on {addr}");

    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

#[tracing::instrument(skip(state))]
fn app(state: AppState) -> Router {
    let x_request_id = HeaderName::from_static("x-request-id");
    let cache_control = HeaderName::from_static("cache-control");

    Router::new()
        .nest("/ovarit_auth", o_auth::ovarit_auth(state.auth_state.clone()))
        .route_layer(from_fn_with_state(
            Arc::new(Mutex::new(RateLimiter::new(10, Duration::from_secs(60)))),
            rate_limiter::rate_limiter,
        ))
        .nest("/ovarit_post", post_endpoints::post_endpoints(state.clone()))
        .route_service("/", ServeFile::new("static/root.html"))
        .route_service("/login", ServeFile::new("static/login.html"))
        .route_service("/register", ServeFile::new("static/register.html"))
        .route_service("/confirmation", ServeFile::new("static/confirmation.html"))
        .route_service("/reset", ServeFile::new("static/reset.html"))
        .route_service(
            "/reset_complete",
            ServeFile::new("static/reset_complete.html"),
        )
        .route_service("/resend", ServeFile::new("static/resend.html"))
        .route_service("/update", ServeFile::new("static/update.html"))
        .nest_service("/ovarit_auth/static", ServeDir::new("static"))
        .layer(CookieManagerLayer::new())
        .layer(SetResponseHeaderLayer::if_not_present(
            cache_control,
            Some(HeaderValue::from_static("no-store")),
        ))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with({
                    let x_request_id = x_request_id.clone();
                    move |req: &Request<Body>| {
                        // ip addresses in x-forwarded-for are comma separated, so we only take the first one
                        let ip_address = req
                            .headers()
                            .get("x-forwarded-for")
                            .and_then(|ip| ip.to_str().ok())
                            .and_then(|ip| ip.split(',').next())
                            .and_then(|ip| {
                                let addr: IpAddr = ip.parse().ok()?;
                                match addr {
                                    IpAddr::V4(addr) => Some(addr.to_ipv6_mapped().to_string()),
                                    IpAddr::V6(addr) => Some(addr.to_string()),
                                }
                            })
                            .unwrap_or("unknown".to_string());

                        tracing::info_span!(
                            "http_request",
                            "ip_address" = ip_address,
                            "request_id" = req.headers().get(x_request_id.clone()).and_then(|v| v.to_str().ok()).unwrap_or("unknown"),
                            "method" = ?req.method(),
                            "path" = req.uri().path(),
                            "query" = req.uri().query(),
                            "username" = tracing::field::Empty,
                            "email" = tracing::field::Empty,
                        )
                    }
                })
                .on_request(DefaultOnRequest::new().level(Level::INFO))
                .on_response(
                    DefaultOnResponse::new()
                        .level(Level::INFO)
                        .latency_unit(LatencyUnit::Millis),
                )
                .on_failure(DefaultOnFailure::new().level(Level::ERROR)),
        )
        .layer(CompressionLayer::new())
        .layer(CatchPanicLayer::custom(|err: Box<dyn Any + Send>| {
            let details = if let Some(s) = err.downcast_ref::<String>() {
                s.clone()
            } else { match err.downcast_ref::<&str>() { Some(s) => {
                            s.to_string()
                        } _ => {
                            "Unknown panic message".to_string()
                        }}};

            error!("Panic: {details}");

            let body = serde_json::json!({
                "error": {
                    "kind": "panic",
                    "details": details,
                }
            });
            let body = serde_json::to_string(&body).unwrap();

            Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(body))
                .unwrap()
        }))
        .layer(SetRequestIdLayer::new(
            x_request_id.clone(),
            MakeRequestUuid,
        ))
        .layer(PropagateHeaderLayer::new(x_request_id.clone()))
        .route("/health", get(health))
}

async fn health() -> &'static str {
    "OK"
}

mod rate_limiter {
    use axum::{
        body::Body,
        extract::State,
        http::{Request, StatusCode},
        middleware::Next,
        response::{IntoResponse, Response},
    };
    use ovarit_auth::rate_limiter::RateLimiter;
    use std::{
        net::{IpAddr, Ipv6Addr},
        sync::{Arc, Mutex},
    };
    use tracing::error;

    #[tracing::instrument(skip(state, request, next))]
    pub async fn rate_limiter(
        State(state): State<Arc<Mutex<RateLimiter<Ipv6Addr>>>>,
        request: Request<Body>,
        next: Next,
    ) -> Response {
        let ip = request
            .headers()
            .get("X-Forwarded-For")
            .and_then(|x| x.to_str().ok())
            .and_then(|x| x.split(',').next());
        let ip = ip.and_then(|x| x.parse::<IpAddr>().ok()).map(|x| match x {
            IpAddr::V4(x) => x.to_ipv6_mapped(),
            IpAddr::V6(x) => x,
        });

        if let Some(ip) = ip {
            if state.lock().unwrap().limit(&ip) {
                error!("Rate limit exceeded for ip: {}", ip);
                return (StatusCode::TOO_MANY_REQUESTS, ()).into_response();
            }
        };

        next.run(request).await
    }
}

#[cfg(test)]
mod test;
