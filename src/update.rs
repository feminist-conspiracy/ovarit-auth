use crate::{
    authenticate::AuthenticationError,
    create::{validate_email, validate_password},
    rate_limiter::{limit, RateLimiter, ONE_DAY},
    users::{
        hash_password, is_valid_username, verify_password, ContactType, DataStore, EmailError,
        User, UserContact, UserError, UserPendingAction, UserStatus,
    },
};
use serde::Deserialize;
use std::{
    fmt::Debug,
    sync::Arc,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use thiserror::Error;
use uuid::Uuid;

#[derive(Error, Debug)]
pub enum ResetError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] UserError),
    #[error("Error in the contact subsystem: {0}")]
    Contact(#[from] EmailError),
    #[error("Error in the authentication subsystem: {0}")]
    Authenticate(#[from] AuthenticationError),
    #[error("Rate limit exceeded")]
    RateLimit,
}

#[derive(Deserialize, Debug)]
pub struct ResetRequest {
    username: String,
    email: String,
}

/// Starts the reset process, sends an email to the given email address with a link to the reset page.
/// Email is parameterized with the "magic code" rather than a full link.
#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn start_reset(
    req: ResetRequest,
    user_datastore: &Arc<dyn DataStore + Send + Sync>,
    user_contact: &Arc<dyn UserContact + Send + Sync>,
) -> Result<(), ResetError> {
    if limit("reset", &req.username, || RateLimiter::new(2, ONE_DAY)) {
        return Err(ResetError::RateLimit);
    }

    let code = Uuid::new_v4().as_hyphenated().to_string();
    let user = user_datastore.get_user_by_username(&req.username).await?;
    if user.status != UserStatus::Normal {
        return Err(ResetError::User(UserError::InvalidState));
    }
    if let Some(UserPendingAction::Verify { .. }) = user.pending_action {
        return Err(ResetError::User(UserError::InvalidState));
    }
    if user.email.to_lowercase() != req.email.to_lowercase() {
        return Err(ResetError::User(UserError::NotFound));
    }
    let user = User {
        pending_action: Some(UserPendingAction::Reset {
            code: code.clone(),
            time: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        }),
        ..user
    };
    user_datastore.update_user(&user).await?;
    user_contact
        .send_email(
            ContactType::Reset(&user.username),
            &user.email,
            &user.uid,
            &code,
        )
        .await?;
    Ok(())
}

#[derive(Deserialize)]
pub struct CompleteReset {
    code: String,
    new_password: String,
}

impl Debug for CompleteReset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CompleteReset")
            .field("code", &self.code)
            .field("new_password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(user_datastore))]
pub async fn complete_reset(
    req: CompleteReset,
    user_datastore: &Arc<dyn DataStore + Send + Sync>,
) -> Result<User, ResetError> {
    let user = user_datastore.get_user_by_code(&req.code).await?;

    let user = match user.pending_action {
        Some(UserPendingAction::Reset { code, time })
            if code == req.code
                && (SystemTime::now().duration_since(UNIX_EPOCH).unwrap()
                    - Duration::from_secs(time)
                    < Duration::from_secs(60 * 60 * 12)) =>
        {
            User {
                pending_action: None,
                parsed_hash: hash_password(&req.new_password)?,
                reset_count: user.reset_count + 1,
                ..user
            }
        }
        _ => return Err(ResetError::User(UserError::InvalidState)),
    };
    user_datastore.update_user(&user).await?;
    Ok(user)
}

#[derive(Deserialize)]
pub struct UpdateRequest {
    pub uid: String,
    pub password: String,
    pub new_email: Option<String>,
    pub new_password: Option<String>,
}

impl Debug for UpdateRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("UpdateRequest")
            .field("uid", &self.uid)
            .field("password", &"***")
            .field("new_email", &self.new_email)
            .field("new_password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn update(
    req: UpdateRequest,
    user_datastore: &Arc<dyn DataStore + Send + Sync>,
    user_contact: &Arc<dyn UserContact + Send + Sync>,
) -> Result<User, ResetError> {
    let user = user_datastore.get_user_by_uid(&req.uid).await?;
    verify_password(&req.password, &user.parsed_hash).await?;
    if let Some(UserPendingAction::Verify { .. }) = user.pending_action {
        return Err(ResetError::Authenticate(AuthenticationError::Unverified));
    }

    let code = Uuid::new_v4().as_hyphenated().to_string();
    let use_new_email = if let Some(new_email) = req.new_email.as_ref() {
        validate_email(new_email)?;
        new_email != &user.email
    } else {
        false
    };

    let user = User {
        pending_action: if use_new_email {
            Some(UserPendingAction::NewEmail {
                email: req.new_email.clone().unwrap(),
                code: code.clone(),
            })
        } else {
            user.pending_action
        },
        parsed_hash: if let Some(ref new_password) = req.new_password {
            validate_password(new_password)?;
            hash_password(new_password)?
        } else {
            user.parsed_hash
        },
        reset_count: if req.new_password.is_some() {
            user.reset_count + 1
        } else {
            user.reset_count
        },
        ..user
    };

    user_datastore.update_user(&user).await?;
    if use_new_email {
        user_contact
            .send_email(
                ContactType::Update,
                &req.new_email.unwrap(),
                &user.uid,
                &code,
            )
            .await?;
    }
    Ok(user)
}

#[derive(Deserialize)]
pub struct ChangeUsernameRequest {
    pub username: String,
    pub password: String,
    pub new_username: String,
}

impl Debug for ChangeUsernameRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ChangeUsernameRequest")
            .field("username", &self.username)
            .field("password", &"***")
            .field("new_name", &self.new_username)
            .finish()
    }
}

#[derive(Error, Debug)]
pub enum ChangeUsernameError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] UserError),
    #[error("Error in the authentication subsystem: {0}")]
    Authenticate(#[from] AuthenticationError),
}

#[tracing::instrument(skip(user_datastore))]
pub async fn change_username(
    req: ChangeUsernameRequest,
    user_datastore: &Arc<dyn DataStore + Send + Sync>,
) -> Result<User, ChangeUsernameError> {
    let user = user_datastore.get_user_by_username(&req.username).await?;
    verify_password(&req.password, &user.parsed_hash).await?;
    if let Some(UserPendingAction::Verify { .. }) = user.pending_action {
        return Err(ChangeUsernameError::Authenticate(
            AuthenticationError::Unverified,
        ));
    }

    if !is_valid_username(&req.new_username) {
        return Err(ChangeUsernameError::User(UserError::InvalidUsername));
    }

    let user = User {
        username: req.new_username,
        ..user
    };

    user_datastore.change_username(&user).await?;
    Ok(user)
}

#[tracing::instrument(skip(user_store))]
pub async fn retire(
    uid: &str,
    password: String,
    user_store: &Arc<dyn DataStore + Send + Sync>,
) -> Result<(), ResetError> {
    let user = user_store.get_user_by_uid(uid).await?;
    verify_password(&password, &user.parsed_hash).await?;
    user_store.soft_delete(&user.uid).await?;
    Ok(())
}

#[derive(Deserialize, Debug)]
pub struct DeleteRequest {
    pub uid: String,
    pub code: String,
}

#[tracing::instrument(skip(user_datastore))]
pub async fn delete(
    req: DeleteRequest,
    user_datastore: &Arc<dyn DataStore + Send + Sync>,
) -> Result<(), ResetError> {
    let user = user_datastore.get_user_by_uid(&req.uid).await?;

    let is_valid_code = match user.pending_action {
        Some(UserPendingAction::Verify { delete, .. }) => delete == req.code,
        _ => return Err(ResetError::User(UserError::InvalidState)),
    };

    if !is_valid_code {
        return Err(ResetError::User(UserError::InvalidState));
    }

    user_datastore.delete_user(&req.uid).await?;
    Ok(())
}

#[cfg(test)]
mod test {
    use std::{collections::HashMap, sync::Arc};

    use tokio::sync::Mutex;

    use crate::{create::validate, users::in_memory::InMemoryDataStore};

    use super::*;

    struct HashMapContact {
        map: Mutex<HashMap<String, String>>,
    }
    #[async_trait::async_trait]
    impl UserContact for HashMapContact {
        async fn send_email(
            &self,
            _ty: ContactType<'_>,
            _email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), EmailError> {
            self.map
                .lock()
                .await
                .insert(uid.to_string(), code.to_string());
            Ok(())
        }
    }

    #[tokio::test]
    async fn test_update() {
        let store = Arc::new(InMemoryDataStore::default());
        let contact = Arc::new(HashMapContact {
            map: Mutex::new(HashMap::new()),
        });

        // create a user and store it
        let user = User {
            uid: "test".to_string(),
            username: "test".to_string(),
            email: "test@test.test".to_string(),
            status: UserStatus::Normal,
            pending_action: None,
            parsed_hash: hash_password("dumb_password1").unwrap(),
            reset_count: 0,
            required_name_change_message: None,
        };
        store.write_user("", &user).await.unwrap();

        // update the user
        let update = UpdateRequest {
            uid: "test".to_string(),
            password: "dumb_password1".to_string(),
            new_email: Some("test2@test.test".to_string()),
            new_password: Some("dumb_password2".to_string()),
        };
        super::update(update, &(store.clone() as _), &(contact.clone() as _))
            .await
            .unwrap();

        // check that the user was updated
        let updated_user = store.get_user_by_username("test").await.unwrap();
        assert!(verify_password("dumb_password2", &updated_user.parsed_hash)
            .await
            .is_ok());

        // But that the email address didn't change.
        assert_eq!(updated_user.email, "test@test.test");

        // check that the user was contacted
        let code = contact.map.lock().await.get("test").cloned().unwrap();
        assert!(!code.is_empty());

        // complete the email verification
        let res = validate(code, &(store.clone() as _)).await;
        assert!(res.is_ok());

        // check that the user was updated
        let updated_user = store.get_user_by_username("test").await.unwrap();
        assert_eq!(updated_user.email, "test2@test.test");
    }

    #[tokio::test]
    async fn test_change_username() {
        let store = Arc::new(InMemoryDataStore::default());

        // create a user and store it
        let user = User {
            uid: "test".to_string(),
            username: "test".to_string(),
            email: "test@test.test".to_string(),
            status: UserStatus::Normal,
            pending_action: None,
            parsed_hash: hash_password("dumb_password1").unwrap(),
            reset_count: 0,
            required_name_change_message: None,
        };
        store.write_user("", &user).await.unwrap();

        // change the user name
        let change = ChangeUsernameRequest {
            username: "test".to_string(),
            password: "dumb_password1".to_string(),
            new_username: "test2".to_string(),
        };
        super::change_username(change, &(store.clone() as _))
            .await
            .unwrap();

        // check that the user was updated
        let updated_user = store.get_user_by_username("test2").await.unwrap();

        // check that other fields are unchanged
        assert_eq!(updated_user.uid, "test");
        assert!(verify_password("dumb_password1", &updated_user.parsed_hash)
            .await
            .is_ok());
        assert_eq!(updated_user.email, "test@test.test");
    }
}
