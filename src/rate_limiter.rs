use std::{
    collections::HashMap,
    time::{Duration, Instant},
};

pub const ONE_DAY: Duration = Duration::from_secs(60 * 60 * 24);
static RATE_LIMITER: once_cell::sync::Lazy<std::sync::Mutex<HashMap<String, RateLimiter<String>>>> =
    once_cell::sync::Lazy::new(|| std::sync::Mutex::new(HashMap::new()));

/// Rate limiter
/// This is a simple rate limiter that keeps track of the number of requests for a given key
/// and limits the number of requests to a given number in a given duration
#[derive(Clone, Debug)]
pub struct RateLimiter<T> {
    duration: Duration,
    max_requests: u32,
    storage: HashMap<T, Vec<Instant>>,
}

impl<T> RateLimiter<T> {
    /// Create a new rate limiter
    pub fn new(max_requests: u32, duration: Duration) -> Self {
        Self {
            duration,
            max_requests,
            storage: HashMap::new(),
        }
    }

    /// Limit the number of requests for a given key
    /// Returns true if the limit is exceeded
    pub fn limit(&mut self, key: &T) -> bool
    where
        T: std::hash::Hash + Eq + Clone,
    {
        self.clean();
        let now = Instant::now();
        let requests = self.storage.entry(key.to_owned()).or_default();
        if requests.len() as u32 >= self.max_requests {
            return true;
        }
        requests.push(now);
        false
    }

    /// Clean up the storage by removing all the requests that are older than the duration
    /// This is a best effort approach, it is not guaranteed that all the requests will be removed
    pub fn clean(&mut self) {
        let now = Instant::now();
        for (_, requests) in self.storage.iter_mut() {
            requests.retain(|&t| t > now - self.duration);
        }
    }
}

pub fn limit<T>(name: &str, key: &String, constructor: T) -> bool
where
    T: Fn() -> RateLimiter<String>,
{
    let mut limiter = RATE_LIMITER.lock().unwrap();
    let limiter = limiter.entry(name.to_string()).or_insert_with(constructor);
    limiter.limit(key)
}
