//! Library to support the basic operations for a auth system
//! Must be able to:
//!   * create a user
//!   * resend a validation email
//!   * validate an email
//!   * authenticate a user
//!   * reset a password
//!   * update a password/email

use std::sync::Arc;

pub mod authenticate;

pub mod create;

pub mod update;

pub mod users;

pub mod flask_session;
pub mod rate_limiter;

pub mod post;

#[derive(Clone)]
pub struct AuthState {
    pub user_store: Arc<dyn users::DataStore + Send + Sync>,
    pub user_contact: Arc<dyn users::UserContact + Send + Sync>,
}
