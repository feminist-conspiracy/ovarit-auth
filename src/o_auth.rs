use axum::{
    extract::State,
    http::{header, HeaderMap, HeaderValue, StatusCode},
    response::{IntoResponse, Response},
    routing::{get, post},
    Json, Router,
};
use eyre::Result;
use ovarit_auth::{
    authenticate::{self, AuthenticationError},
    create::{self, CreateError},
    flask_session::{encode_session, remember_me, Session},
    update,
    users::{self, UserError, UserPendingAction, UserStatus},
    AuthState,
};
use serde::Deserialize;
use serde_json::json;
use std::fmt::Debug;
use std::fmt::{self, Formatter};
use tracing::{debug, error, warn};

#[tracing::instrument(skip(auth_state))]
pub fn ovarit_auth(auth_state: AuthState) -> Router {
    Router::new()
        .route("/authenticate", post(login))
        .route("/change_username", post(change_username))
        .route("/create", post(create))
        .route("/create/validate", post(validate))
        .route("/create/resend", post(resend))
        .route("/reset", post(reset))
        .route("/reset/complete", post(reset_complete))
        .route("/update", post(update))
        .route("/retire", post(retire))
        .route("/delete", post(delete))
        .route("/info", get(info))
        .with_state(auth_state)
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip(state))]
async fn login(
    State(state): State<AuthState>,
    Json(login): Json<authenticate::Authenticate>,
) -> Response {
    let rm = login.remember_me;
    let res = authenticate::authenticate(&login, &state.user_store).await;

    match res {
        Ok(user) => {
            debug!("Authenticated user {user:#?}");
            match user.status {
                UserStatus::Banned => {
                    return (
                        StatusCode::FORBIDDEN,
                        Json(json! ({ "reason" : "You have been banned from the site."})),
                    )
                        .into_response();
                }
                UserStatus::Deleted => {
                    return (
                        StatusCode::NOT_FOUND,
                        Json(json!({"reason": "Your account has been deleted."})),
                    )
                        .into_response()
                }
                _ => (),
            }
            if let Some(UserPendingAction::Verify { .. }) = user.pending_action {
                return (
                StatusCode::FORBIDDEN,
                Json(json! ({"reason": "Please click the link in the email we sent you to verify your account."})),
            )
                .into_response();
            }

            let session = Session {
                _fresh: true,
                remember_me: rm,
                _id: None,
                language: None,
                _user_id: if user.reset_count != 0 {
                    format!("{}${}", user.uid, user.reset_count)
                } else {
                    user.uid.clone()
                },
                csrf_token: uuid::Uuid::new_v4().to_string(),
            };

            let headers = build_session_headers(&session);

            match headers {
                Ok(headers) => (StatusCode::ACCEPTED, headers, ()).into_response(),
                Err(e) => e,
            }
        }
        Err(AuthenticationError::BadPassword) => {
            warn!(
                "Failed to authenticate due to bad password for user {}",
                login.username
            );
            (
                StatusCode::UNAUTHORIZED,
                Json(json! ({ "reason" : "Incorrect password"})),
            )
                .into_response()
        }
        Err(
            AuthenticationError::Unverified | AuthenticationError::User(UserError::InvalidState),
        ) => {
            warn!(
                "Failed to authenticate due to unverified user {}",
                login.username
            );
            (
        StatusCode::FORBIDDEN,
        Json(json! ({ "reason" : "Please click the link in the email we sent you to verify your account."})),
    )
        .into_response()
        }
        Err(AuthenticationError::User(UserError::NotFound)) => {
            warn!(
                "Failed to authenticate due to user not found {}",
                login.username
            );
            (
                StatusCode::NOT_FOUND,
                Json(json! ({ "reason" : "User not found"})),
            )
                .into_response()
        }
        Err(AuthenticationError::User(UserError::PasswordHashingError(e))) => {
            error!("Failed to authenticate due to password hashing: {e}");
            (
                StatusCode::EXPECTATION_FAILED,
                Json(json!({"reason": "Password reset required."})),
            )
                .into_response()
        }
        Err(AuthenticationError::NameChangeRequired(message)) => {
            warn!(
                "Failed to authenticate due to required name change: {}",
                login.username
            );
            (
                StatusCode::FORBIDDEN,
                Json(json! ({"reason": "Name change required",
                             "message": message})),
            )
                .into_response()
        }
        Err(AuthenticationError::User(
            UserError::Exists
            | UserError::BadCode
            | UserError::BannedEmailDomain
            | UserError::InvalidEmail
            | UserError::InvalidUsername
            | UserError::PasswordComplexity
            | UserError::RegistrationDisabled
            | UserError::BannedUsername(_),
        )) => {
            panic!("Only in write path")
        }
        Err(AuthenticationError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to authenticate: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
                .into_response()
        }
        Err(AuthenticationError::User(UserError::DBError(_))) => {
            error!("Failed to authenticate: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({"reason": "An internal error occurred"})),
            )
                .into_response()
        }
    }
}

fn build_session_headers(session: &Session) -> Result<HeaderMap, Response> {
    let session_result = encode_session(session);

    let session_cookie = match session_result {
        Ok(s) => s,
        Err(e) => return Err((e.0, Json(json! ({"reason": "Session un-encoded"}))).into_response()),
    };

    let mut headers = HeaderMap::new();
    headers.append(
        header::SET_COOKIE,
        HeaderValue::from_str(&format!("session={session_cookie}; Path=/; HttpOnly")).unwrap(),
    );

    if session.remember_me {
        let rm_cookie = remember_me(session.get_user_id(), session.get_reset_count());
        headers.append(
            header::SET_COOKIE,
            HeaderValue::from_str(&format!(
                "remember_token={rm_cookie}; Path=/; HttpOnly; Max-Age=31536000"
            ))
            .unwrap(),
        );
    };
    Ok(headers)
}

#[tracing::instrument(skip(state))]
async fn create(
    State(state): State<AuthState>,
    Json(create): Json<create::NewUser>,
) -> impl IntoResponse {
    let res = create::create(create, &state.user_store, &state.user_contact).await;

    match res {
        Ok((uid, code)) => (
            StatusCode::CREATED,
            Json(json!({ "uid": uid, "code": code })),
        ),
        Err(CreateError::User(UserError::Exists)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Username already in use" })),
        ),
        Err(CreateError::User(UserError::BadCode)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid invite code" })),
        ),
        Err(CreateError::User(UserError::BannedEmailDomain)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "We do not accept email from your email provider." })),
        ),
        Err(CreateError::User(UserError::BannedUsername(banned))) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Username contains banned characters", "banned": banned })),
        ),
        Err(CreateError::User(UserError::InvalidEmail)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid email address" })),
        ),
        Err(CreateError::User(UserError::InvalidUsername)) => (
            StatusCode::BAD_REQUEST,
            Json(
                json!({ "reason": "Usernames should be at least 3 characters long, and may contain alphanumeric characters plus '-' and '_'." }),
            ),
        ),
        Err(CreateError::User(UserError::PasswordComplexity)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Password does not meet complexity requirements" })),
        ),
        Err(CreateError::User(UserError::RegistrationDisabled)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Registration is currently disabled" })),
        ),
        Err(CreateError::User(UserError::NotFound)) => {
            panic!("Of course it's not found")
        }
        Err(CreateError::Email(_)) => {
            error!("Failed to create user: {res:?}");
            (StatusCode::BAD_GATEWAY, {
                Json(json!({ "reason": "Email service error" }))
            })
        }
        Err(CreateError::User(UserError::PasswordHashingError(_))) => {
            error!("Failed to create user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
        }
        Err(CreateError::User(UserError::InvalidState)) => {
            panic!("Only in update path")
        }
        Err(CreateError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to create user: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
        }
        Err(CreateError::User(UserError::DBError(_))) => {
            error!("Failed to create user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Validate {
    code: String,
}

#[tracing::instrument(skip(state))]
async fn validate(State(state): State<AuthState>, Json(validate): Json<Validate>) -> Response {
    let res = create::validate(validate.code, &state.user_store).await;

    match res {
        Ok(_) => (StatusCode::OK, Json(json!({}))).into_response(),
        Err(create::ValidateError::NoKey)
        | Err(create::ValidateError::User(UserError::NotFound | UserError::Exists)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found or already validated" })),
        )
            .into_response(),
        Err(create::ValidateError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "User state does not permit validation" })),
        )
            .into_response(),
        Err(create::ValidateError::User(UserError::PasswordHashingError(_))) => {
            error!("Failed to validate user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
        Err(create::ValidateError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to validate user: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
                .into_response()
        }
        Err(create::ValidateError::User(
            UserError::DBError(_)
            | UserError::BadCode
            | UserError::BannedEmailDomain
            | UserError::InvalidEmail
            | UserError::InvalidUsername
            | UserError::PasswordComplexity
            | UserError::RegistrationDisabled
            | UserError::BannedUsername(_),
        )) => {
            error!("Failed to validate user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
    }
}

#[tracing::instrument(skip(state))]
async fn resend(
    State(state): State<AuthState>,
    Json(req): Json<create::ResendRequest>,
) -> impl IntoResponse {
    let res = create::resend(req, &state.user_store, &state.user_contact).await;

    match res {
        Ok(_uid) => (
            StatusCode::OK,
            Json(json!({ "reason": "Email sent successfully" })),
        ),
        Err(create::ResendError::EmailError(_)) => {
            error!("Failed to resend validation email: {res:?}");
            (
                StatusCode::GATEWAY_TIMEOUT,
                Json(json!({ "reason": "Failed to send email, please try again later" })),
            )
        }
        Err(create::ResendError::RateLimit) => (
            StatusCode::TOO_MANY_REQUESTS,
            Json(json!({ "reason": "Too many requests, please try again later" })),
        ),
        Err(create::ResendError::User(_) | create::ResendError::UserVerified) => (
            StatusCode::OK,
            Json(json!({ "reason": "Email sent successfully or user already verified" })),
        ),
    }
}

#[tracing::instrument(skip(state))]
async fn reset(
    State(state): State<AuthState>,
    Json(req): Json<update::ResetRequest>,
) -> impl IntoResponse {
    let res = update::start_reset(req, &state.user_store, &state.user_contact).await;

    match res {
        Ok(_) => (StatusCode::OK, ()),
        Err(update::ResetError::User(_)) => (StatusCode::OK, ()),
        Err(update::ResetError::Contact(_)) => (StatusCode::OK, ()),
        Err(update::ResetError::Authenticate(_)) => (StatusCode::OK, ()),
        Err(update::ResetError::RateLimit) => (StatusCode::TOO_MANY_REQUESTS, ()),
    }
}

#[tracing::instrument(skip(state))]
async fn reset_complete(
    State(state): State<AuthState>,
    Json(req): Json<update::CompleteReset>,
) -> Response {
    let res = update::complete_reset(req, &state.user_store).await;

    match res {
        Ok(_) => (
            StatusCode::OK,
            Json(json!({ "reason": "Password reset successful" })),
        )
            .into_response(),
        Err(update::ResetError::User(
            UserError::DBError(sqlx::Error::Database(_))
            | UserError::BadCode
            | UserError::PasswordHashingError(_),
        )) => {
            error!("Failed to complete reset: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
        Err(update::ResetError::User(UserError::DBError(_))) => {
            error!("Failed to complete reset: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "Temporary service error" })),
            )
                .into_response()
        }
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for password reset" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "User not found" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::Exists)) => (
            StatusCode::CONFLICT,
            Json(json!({ "reason": "User already exists" })),
        )
            .into_response(),
        Err(update::ResetError::Authenticate(_)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Authentication failed" })),
        )
            .into_response(),
        Err(
            update::ResetError::Contact(_)
            | update::ResetError::RateLimit
            | update::ResetError::User(
                UserError::BannedEmailDomain
                | UserError::InvalidEmail
                | UserError::InvalidUsername
                | UserError::PasswordComplexity
                | UserError::RegistrationDisabled
                | UserError::BannedUsername(_),
            ),
        ) => {
            unimplemented!("This should only happen on the start, not the complete")
        }
    }
}

#[derive(Deserialize)]
pub struct UpdateRequest {
    password: String,
    new_email: Option<String>,
    new_password: Option<String>,
}

impl Debug for UpdateRequest {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("UpdateRequest")
            .field("password", &"***")
            .field("new_email", &self.new_email)
            .field("new_password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(state))]
#[axum_macros::debug_handler]
async fn info(session: Session, State(state): State<AuthState>) -> Response {
    println!("Getting user info: {:#?}", session);
    let res = users::info(session.get_user_id(), &state.user_store).await;

    match res {
        Ok(user) => (StatusCode::OK, Json(user)).into_response(),
        Err(
            UserError::DBError(_)
            | UserError::BadCode
            | UserError::BannedEmailDomain
            | UserError::InvalidEmail
            | UserError::InvalidUsername
            | UserError::PasswordComplexity
            | UserError::RegistrationDisabled
            | UserError::BannedUsername(_)
            | UserError::Exists
            | UserError::PasswordHashingError(_)
            | UserError::InvalidState,
        ) => {
            error!("Failed to get user info: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
        Err(UserError::NotFound) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        )
            .into_response(),
    }
}

#[derive(Deserialize, Debug)]
struct RetireRequest {
    password: String,
}

#[tracing::instrument(skip(state))]
#[axum_macros::debug_handler]
async fn retire(
    session: Session,
    State(state): State<AuthState>,
    Json(req): Json<RetireRequest>,
) -> impl IntoResponse {
    println!("Updating user: {:#?}", session);
    let res = update::retire(session.get_user_id(), req.password, &state.user_store).await;

    match res {
        Ok(_) | Err(update::ResetError::User(UserError::RegistrationDisabled)) => (
            StatusCode::OK,
            Json(json!({ "reason": "User update successful" })),
        ),
        Err(update::ResetError::Authenticate(AuthenticationError::BadPassword)) => (
            StatusCode::UNAUTHORIZED,
            Json(json!({ "reason": "Incorrect password" })),
        ),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        ),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for update" })),
        ),
        Err(update::ResetError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
        }
        Err(update::ResetError::User(UserError::PasswordHashingError(_)))
        | Err(update::ResetError::User(UserError::DBError(_))) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
        }
        Err(update::ResetError::Contact(_)) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::GATEWAY_TIMEOUT,
                Json(json!({ "reason": "Contact service not available" })),
            )
        }
        Err(update::ResetError::User(UserError::InvalidEmail)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid email" })),
        ),
        Err(update::ResetError::User(
            UserError::InvalidUsername | UserError::BannedUsername(_),
        )) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid username" })),
        ),
        Err(update::ResetError::User(UserError::PasswordComplexity)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Password complexity requirements not met" })),
        ),
        Err(update::ResetError::User(UserError::Exists))
        | Err(update::ResetError::User(UserError::BadCode))
        | Err(update::ResetError::Authenticate(AuthenticationError::User(_)))
        | Err(update::ResetError::Authenticate(AuthenticationError::NameChangeRequired(_))) => {
            panic!("This is only in the write path")
        }
        Err(update::ResetError::Authenticate(AuthenticationError::Unverified))
        | Err(update::ResetError::User(UserError::BannedEmailDomain)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Forbidden operation" })),
        ),
        Err(update::ResetError::RateLimit) => (
            StatusCode::TOO_MANY_REQUESTS,
            Json(json!({ "reason": "Too many requests" })),
        ),
    }
}

#[tracing::instrument(skip(state))]
#[axum_macros::debug_handler]
async fn update(
    session: Session,
    State(state): State<AuthState>,
    Json(req): Json<UpdateRequest>,
) -> impl IntoResponse {
    println!("Updating user: {:#?}", session);
    let res = update::update(
        update::UpdateRequest {
            uid: session.get_user_id().to_owned(),
            password: req.password,
            new_email: req.new_email,
            new_password: req.new_password,
        },
        &state.user_store,
        &state.user_contact,
    )
    .await;

    match res {
        Err(update::ResetError::User(UserError::RegistrationDisabled)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Registration is disabled" })),
        )
            .into_response(),
        Ok(user) => {
            let session = Session {
                _user_id: if user.reset_count != 0 {
                    format!("{}${}", user.uid, user.reset_count)
                } else {
                    user.uid.clone()
                },
                ..session
            };
            let headers = build_session_headers(&session);

            match headers {
                Ok(headers) => (
                    StatusCode::OK,
                    headers,
                    Json(json!({ "reason": "User update successful" })),
                )
                    .into_response(),
                Err(_) => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Json(json!({ "reason": "Failed to build session headers" })),
                )
                    .into_response(),
            }
        }
        Err(update::ResetError::Authenticate(AuthenticationError::BadPassword)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Incorrect password" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for update" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
                .into_response()
        }
        Err(update::ResetError::User(UserError::PasswordHashingError(_)))
        | Err(update::ResetError::User(UserError::DBError(_))) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
        Err(update::ResetError::Contact(_)) => {
            error!("Failed to update user: {res:?}");
            (
                StatusCode::GATEWAY_TIMEOUT,
                Json(json!({ "reason": "Contact service not available" })),
            )
                .into_response()
        }
        Err(update::ResetError::User(UserError::InvalidEmail)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid email" })),
        )
            .into_response(),
        Err(update::ResetError::User(
            UserError::InvalidUsername | UserError::BannedUsername(_),
        )) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid username" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::PasswordComplexity)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Password complexity requirements not met" })),
        )
            .into_response(),
        Err(update::ResetError::User(UserError::Exists))
        | Err(update::ResetError::User(UserError::BadCode))
        | Err(update::ResetError::Authenticate(AuthenticationError::User(_)))
        | Err(update::ResetError::Authenticate(AuthenticationError::NameChangeRequired(_))) => {
            panic!("This is only in the write path")
        }
        Err(update::ResetError::Authenticate(AuthenticationError::Unverified))
        | Err(update::ResetError::User(UserError::BannedEmailDomain)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Forbidden operation" })),
        )
            .into_response(),
        Err(update::ResetError::RateLimit) => (
            StatusCode::TOO_MANY_REQUESTS,
            Json(json!({ "reason": "Too many requests" })),
        )
            .into_response(),
    }
}

#[derive(Deserialize)]
pub struct ChangeUsernameRequest {
    username: String,
    password: String,
    new_username: String,
}

impl Debug for ChangeUsernameRequest {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("ChangeUsernameRequest")
            .field("username", &self.username)
            .field("password", &"***")
            .field("new_username", &self.new_username)
            .finish()
    }
}

#[tracing::instrument(skip(state))]
#[axum_macros::debug_handler]
async fn change_username(
    State(state): State<AuthState>,
    Json(req): Json<ChangeUsernameRequest>,
) -> impl IntoResponse {
    println!(
        "Changing username: {} to {}",
        req.username, req.new_username
    );
    let res = update::change_username(
        update::ChangeUsernameRequest {
            username: req.username,
            password: req.password,
            new_username: req.new_username,
        },
        &state.user_store,
    )
    .await;

    match res {
        Ok(_) => (StatusCode::OK, Json(())).into_response(),

        Err(update::ChangeUsernameError::Authenticate(AuthenticationError::BadPassword)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Incorrect password" })),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "You are not allowed to change your username at this time."})),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::DBError(sqlx::Error::Database(_)))) => {
            error!("Failed to change username: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "Poorly written sql" })),
            )
                .into_response()
        }
        Err(update::ChangeUsernameError::User(UserError::PasswordHashingError(_)))
        | Err(update::ChangeUsernameError::User(UserError::DBError(_))) => {
            error!("Failed to change username: {res:?}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({ "reason": "An internal error occurred" })),
            )
                .into_response()
        }
        Err(update::ChangeUsernameError::User(UserError::BannedUsername(banned))) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Username contains banned characters.", "banned": banned })),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::InvalidUsername)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid username." })),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::Exists)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Username is not available." })),
        )
            .into_response(),
        Err(update::ChangeUsernameError::User(UserError::BadCode))
        | Err(update::ChangeUsernameError::User(UserError::PasswordComplexity))
        | Err(update::ChangeUsernameError::User(UserError::BannedEmailDomain))
        | Err(update::ChangeUsernameError::User(UserError::RegistrationDisabled))
        | Err(update::ChangeUsernameError::User(UserError::InvalidEmail))
        | Err(update::ChangeUsernameError::Authenticate(AuthenticationError::Unverified))
        | Err(update::ChangeUsernameError::Authenticate(AuthenticationError::User(_)))
        | Err(update::ChangeUsernameError::Authenticate(
            AuthenticationError::NameChangeRequired(_),
        )) => {
            error!("This is only in the write path");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "unexpected error" })),
            )
                .into_response()
        }
    }
}

#[tracing::instrument(skip(state))]
async fn delete(
    State(state): State<AuthState>,
    Json(req): Json<update::DeleteRequest>,
) -> impl IntoResponse {
    let res = update::delete(req, &state.user_store).await;

    match res {
        Ok(_) => (
            StatusCode::OK,
            Json(json!({ "reason": "User deleted successfully" })),
        ),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        ),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for deletion" })),
        ),
        _ => {
            error!("Failed to delete user: {res:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "reason": "An internal error occurred" })),
            )
        }
    }
}
