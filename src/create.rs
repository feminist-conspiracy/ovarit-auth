use crate::{
    rate_limiter::{limit, RateLimiter, ONE_DAY},
    users::{self, is_valid_username, User, UserPendingAction},
};
use serde::{Deserialize, Serialize};
use std::{fmt::Debug, sync::Arc};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum CreateError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Error in the email subsystem: {0}")]
    Email(#[from] users::EmailError),
}

#[derive(Deserialize, Serialize)]
pub struct NewUser {
    pub invite_code: String,
    pub username: String,
    pub email: String,
    pub password: String,
}

impl Debug for NewUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NewUser")
            .field("invite_code", &self.invite_code)
            .field("username", &self.username)
            .field("email", &self.email)
            .field("password", &"***")
            .finish()
    }
}

/// Returns the UID of the user in the system
#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn create(
    new_user: NewUser,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(String, String), CreateError> {
    let new_user = NewUser {
        invite_code: new_user.invite_code.trim().to_string(),
        username: new_user.username.trim().to_string(),
        email: new_user.email.trim().to_lowercase(),
        password: new_user.password,
    };

    validate_password(new_user.password.as_str())?;
    validate_email(new_user.email.as_str())?;

    // Check if username is valid, it should be 3-64 characters long and only contain
    // alphanumeric characters, underscores and hyphens.
    if !is_valid_username(&new_user.username) {
        return Err(CreateError::User(users::UserError::InvalidUsername));
    }

    let invite_code = new_user.invite_code.clone();

    let user = user_datastore.validate_new_user(new_user).await?;
    let (delete_code, code) = match &user.pending_action {
        Some(UserPendingAction::Verify { delete, code }) => (delete, code),
        _ => unreachable!("new user should always have a Verify action"),
    };
    user_datastore.write_user(&invite_code, &user).await?;
    user_contact
        .send_email(users::ContactType::Verify, &user.email, &user.uid, code)
        .await?;

    Ok((user.uid, delete_code.clone()))
}

pub fn validate_email(email: &str) -> Result<(), users::UserError> {
    if !email.contains('@') {
        return Err(users::UserError::InvalidEmail);
    }

    let (name, domain) = email
        .rsplit_once("@")
        .ok_or(users::UserError::InvalidEmail)?;

    if name.trim().is_empty() || domain.trim().is_empty() {
        return Err(users::UserError::InvalidEmail);
    }

    // Validate that we have a good domain name
    if !is_valid_domain_name(domain) {
        return Err(users::UserError::InvalidEmail);
    }

    Ok(())
}

fn is_valid_domain_name(domain: &str) -> bool {
    if domain.is_empty() || domain.len() > 253 || domain.starts_with('-') || domain.ends_with('-') {
        return false;
    }
    for label in domain.split('.') {
        if label.is_empty()
            || label.len() > 63
            || label.contains(' ')
            || label
                .chars()
                .any(|c| !c.is_ascii_alphanumeric() && c != '-')
        {
            return false;
        }
    }
    true
}

pub fn validate_password(password: &str) -> Result<(), users::UserError> {
    // Ensure password complexity by verifying it meets 3 of the following conditions:
    // 1. Contains an uppercase letter
    // 2. Contains a lowercase letter
    // 3. Contains a number
    // 4. Contains a special character
    // Additionally, the password must be at least 12 characters long.
    let mut complexity = 0;

    if password.chars().any(char::is_uppercase) {
        complexity += 1;
    }
    if password.chars().any(char::is_lowercase) {
        complexity += 1;
    }
    if password.chars().any(char::is_numeric) {
        complexity += 1;
    }
    if password
        .chars()
        .any(|c| !(c.is_uppercase() || c.is_lowercase() || c.is_numeric()))
    {
        complexity += 1;
    }

    if complexity < 3 || password.len() < 12 {
        return Err(users::UserError::PasswordComplexity);
    }

    Ok(())
}

#[derive(Error, Debug)]
pub enum ValidateError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("No Key")]
    NoKey,
}

#[tracing::instrument(skip(user_datastore))]
pub async fn validate(
    code: String,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
) -> Result<(), ValidateError> {
    let user = user_datastore.get_user_by_code(&code).await?;
    let updated_user = match user.pending_action {
        Some(UserPendingAction::Verify { .. }) => User {
            pending_action: None,
            ..user
        },
        Some(UserPendingAction::NewEmail { email, .. }) => User {
            email,
            pending_action: None,
            ..user
        },
        _ => return Err(ValidateError::NoKey),
    };

    user_datastore.update_user(&updated_user).await?;

    Ok(())
}

#[derive(Error, Debug)]
pub enum ResendError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Email Error: {0}")]
    EmailError(#[from] users::EmailError),
    #[error("User is already verified, nothing to resend")]
    UserVerified,
    #[error("Rate limit exceeded")]
    RateLimit,
}

#[derive(Deserialize, Debug)]
pub struct ResendRequest {
    username: String,
}

#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn resend(
    req: ResendRequest,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(), ResendError> {
    let user = user_datastore.get_user_by_username(&req.username).await?;
    if limit("resend", &user.uid, || RateLimiter::new(2, ONE_DAY)) {
        return Err(ResendError::RateLimit);
    }
    let code = match user.pending_action {
        Some(UserPendingAction::Verify { code, .. }) => code,
        _ => return Err(ResendError::UserVerified),
    };
    user_contact
        .send_email(users::ContactType::Verify, &user.email, &user.uid, &code)
        .await?;

    Ok(())
}

#[cfg(test)]
mod test {
    use std::{collections::HashMap, sync::Arc};

    use tokio::sync::Mutex;

    use crate::{
        authenticate::{Authenticate, AuthenticationError},
        users::{in_memory::InMemoryDataStore, UserContact},
    };

    use super::*;

    struct HashMapContact {
        map: Mutex<HashMap<String, String>>,
    }
    #[async_trait::async_trait]
    impl UserContact for HashMapContact {
        async fn send_email(
            &self,
            _ty: users::ContactType<'_>,
            _email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), users::EmailError> {
            self.map
                .lock()
                .await
                .insert(uid.to_string(), code.to_string());
            Ok(())
        }
    }

    #[tokio::test]
    async fn create_no_validate() {
        let hms = InMemoryDataStore::default();
        let store = Arc::new(hms);
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let username = "nobody".to_string();
        let password = "dumb_password1".to_string();
        let email = "nobody@example.com".to_string();

        let (uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: username.clone(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        let result = crate::authenticate::authenticate(
            &Authenticate {
                password,
                username: username.clone(),
                remember_me: false,
            },
            &(store.clone() as _),
        )
        .await;

        println!("{:?}", result);

        assert!(matches!(result, Err(AuthenticationError::Unverified)));

        assert!(contact.map.lock().await.contains_key(&uid));
    }

    #[tokio::test]
    async fn create_resend() {
        let store = Arc::new(InMemoryDataStore::default());
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });

        let contact2 = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let username = "nobody".to_string();
        let password = "dumb_password1".to_string();
        let email = "nobody@example.com".to_string();

        let (_uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: username.clone(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        resend(
            ResendRequest {
                username: username.clone(),
            },
            &(store.clone() as _),
            &(contact2.clone() as _),
        )
        .await
        .unwrap();

        assert_eq!(
            contact.map.lock().await.get(&username).cloned(),
            contact2.map.lock().await.get(&username).cloned()
        );
    }

    #[tokio::test]
    async fn create_validate() {
        let store = Arc::new(InMemoryDataStore::default());
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let username = "nobody".to_string();
        let password = "dumb_password1".to_string();
        let email = "nobody@example.com".to_string();

        let (uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: username.clone(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        validate(
            contact.map.lock().await.get(&uid).unwrap().to_owned(),
            &(store.clone() as _),
        )
        .await
        .unwrap();

        crate::authenticate::authenticate(
            &Authenticate {
                password,
                username,
                remember_me: false,
            },
            &(store as _),
        )
        .await
        .expect("Login should be successful");
    }
}
