use crate::AppState;
use axum::{extract::State, routing::post, Json, Router};
use hyper::StatusCode;
use ovarit_auth::{
    flask_session::Session,
    post::{self, FileManagementError, ImgId, Sid, TaskManagementError, TitleId, UrlId},
};
use serde::Serialize;
use url::Url;

pub fn post_endpoints(app_state: AppState) -> Router {
    Router::new()
        .route("/upload_url", post(pre_upload))
        .route("/link_info", post(pre_link))
        .with_state(app_state)
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct PreUploadRequest {
    sid: Sid,
    title: String,
    flair: String,
}

#[derive(Serialize, Debug, Clone)]
pub struct PreUploadResponse {
    url: Url,
}

#[derive(Serialize, Debug, Clone)]
pub struct JErr {
    error: String,
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip(state))]
async fn pre_upload(
    session: Session,
    state: State<AppState>,
    body: Json<PreUploadRequest>,
) -> Result<Json<PreUploadResponse>, (StatusCode, Json<JErr>)> {
    let state = state.post_state.clone();
    let url = state
        .file_management
        .create_upload_url(session.get_user_id(), &body.sid, &body.title, &body.flair)
        .await;
    match url {
        Ok(url) => Ok(Json(PreUploadResponse { url })),
        Err(
            e @ (FileManagementError::InvalidFlair
            | FileManagementError::InvalidSid
            | FileManagementError::InvalidTitle),
        ) => {
            tracing::info!("Invalid input: {}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(JErr {
                    error: format!("Invalid Input: {}", e),
                }),
            ))
        }
    }
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct PreLinkRequest {
    url: Url,
}

#[derive(Serialize, Debug, Clone)]
pub struct PreLinkResponse {
    img_id: ImgId,
    url_id: UrlId,
    title_id: TitleId,
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip(state))]
async fn pre_link(
    session: Session,
    state: State<AppState>,
    body: Json<PreLinkRequest>,
) -> Result<Json<PreLinkResponse>, (StatusCode, Json<JErr>)> {
    let state = state.post_state.clone();
    let res: Result<(ImgId, UrlId, TitleId), TaskManagementError> =
        post::pre_link(&body.url, &state).await;

    match res {
        Ok((img_id, url_id, title_id)) => Ok(Json(PreLinkResponse {
            img_id,
            url_id,
            title_id,
        })),
        Err(e) => {
            tracing::info!("Invalid input: {}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(JErr {
                    error: format!("Invalid Input: {}", e),
                }),
            ))
        }
    }
}
