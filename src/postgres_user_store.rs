use super::*;

use async_trait::async_trait;
use ovarit_auth::{
    create::NewUser,
    users::{self, hash_password, User, UserPendingAction, UserStatus},
};
use sqlx::{types::Json, PgPool, Row};
use uuid::Uuid;

#[derive(Clone)]
pub(crate) struct PostgresUserStore {
    pub sql_pool: PgPool,
}

impl PostgresUserStore {
    /// Helper function to fetch all banned email domains from the database
    /// and return them as a vector of strings.
    async fn get_banned_email_domains(&self) -> Result<Vec<String>, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'banned_email_domain'")
                .fetch_all(&self.sql_pool)
                .await?
                .into_iter()
                .map(|row| row.get::<String, _>(0))
                .collect::<Vec<_>>(),
        )
    }

    /// Helper function to fetch all banned usernames from the database
    async fn get_banned_substrings(&self) -> Result<Vec<String>, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'banned_username_string'")
                .fetch_all(&self.sql_pool)
                .await?
                .into_iter()
                .map(|row| row.get::<String, _>(0))
                .collect::<Vec<_>>(),
        )
    }

    /// and return them as a vector of strings.
    async fn find_banned_substring(&self, username: &str) -> Result<Option<String>, sqlx::Error> {
        let banned_substrings = self.get_banned_substrings();
        Ok(banned_substrings
            .await?
            .iter()
            .find(|substring| username.to_lowercase().contains(&substring.to_lowercase()))
            .cloned())
    }

    /// Helper function to fetch username max length.
    async fn get_username_max_length(&self) -> Result<usize, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'site.username_max_length'")
                .fetch_one(&self.sql_pool)
                .await?
                .get::<String, _>(0)
                .parse::<i8>()
                .unwrap_or(64)
                .try_into()
                .unwrap(),
        )
    }

    /// Helper function to check that site registration is enabled.
    async fn is_registration_enabled(&self) -> Result<bool, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'site.enable_registration'")
                .fetch_one(&self.sql_pool)
                .await?
                .get::<String, _>(0)
                .parse::<i8>()
                == Ok(1),
        )
    }

    /// Helper function to check that codes are required for registration.
    async fn is_registration_code_required(&self) -> Result<bool, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'site.require_invite_code'")
                .fetch_one(&self.sql_pool)
                .await?
                .get::<String, _>(0)
                .parse::<i8>()
                == Ok(1),
        )
    }
}

// We have two database tables, one for user information for throat
// and one for user login information for ovarit-auth. We need to
// combine the two to get a full user object.
//
// Table structure is as follows:
//
// throat.user:
// uid: String
// name: String
// status: i32
// resets: i32
//
// ovarit_auth.user:
// uid: String
// email: String
// parsed_hash: String
// state: String
//

#[async_trait]
impl users::DataStore for PostgresUserStore {
    #[tracing::instrument(skip(self))]
    async fn validate_new_user(&self, new_user: NewUser) -> Result<users::User, users::UserError> {
        let max_length = self.get_username_max_length().await?;
        if new_user.username.len() > max_length {
            return Err(users::UserError::InvalidUsername);
        }

        let banned_email_domains = self.get_banned_email_domains().await?;
        if banned_email_domains.iter().any(|domain| {
            let index = new_user.email.to_lowercase().rfind(&domain.to_lowercase());
            let Some(index) = index else {
                return false;
            };

            let before = new_user.email.chars().nth(index - 1).unwrap();

            dbg!(before == '.' || before == '@')
        }) {
            return Err(users::UserError::BannedEmailDomain);
        }

        if let Some(banned_substring) = self.find_banned_substring(&new_user.username).await? {
            return Err(users::UserError::BannedUsername(banned_substring.clone()));
        }

        let delete_code = Uuid::new_v4().as_hyphenated().to_string();
        let code = Uuid::new_v4().as_hyphenated().to_string();
        let user = User {
            uid: Uuid::new_v4().as_hyphenated().to_string(),
            username: new_user.username,
            email: new_user.email,
            parsed_hash: hash_password(&new_user.password)?,
            status: UserStatus::Normal,
            pending_action: Some(UserPendingAction::Verify {
                delete: delete_code.clone(),
                code: code.clone(),
            }),
            reset_count: 0,
            required_name_change_message: None,
        };
        Ok(user)
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_code(&self, code: &str) -> Result<users::User, users::UserError> {
        let (uid, name, email, parsed_hash, pending_action, status, resets, required_name_change_message) = sqlx::query_as::<
            _,
            (
                String,
                String,
                String,
                String,
                Option<Json<UserPendingAction>>,
                i32,
                i32,
                Option<String>,
            ),
        >(
            "SELECT u.uid, u.name, a.email, a.parsed_hash, a.pending_action, u.status, u.resets, required_name_change.message
                    FROM public.user u
                    INNER JOIN user_login a 
                        ON u.uid = a.uid
                    LEFT JOIN required_name_change
                        ON required_name_change.uid = u.uid
                       AND required_name_change.completed_at is null
                    WHERE pending_action->'Verify'->>'code' = $1
                        OR pending_action->'Reset'->>'code' = $1
                        OR pending_action->'NewEmail'->>'code' = $1
                        ",
        )
        .bind(code)
        .fetch_optional(&self.sql_pool)
        .await?
        .ok_or(users::UserError::NotFound)?;

        Ok(users::User {
            uid,
            username: name,
            email,
            parsed_hash,
            status: status.try_into()?,
            pending_action: match pending_action {
                Some(Json(p)) => Some(p),
                _ => None,
            },
            reset_count: resets,
            required_name_change_message,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_uid(&self, uid: &str) -> Result<users::User, users::UserError> {
        // get uid, name, email, parsed_hash, state, and the state from throat.user
        let (uid, name, email, parsed_hash, pending_action, status, resets, required_name_change_message) = sqlx::query_as::<
            _,
            (
                String,
                String,
                String,
                String,
                Option<Json<UserPendingAction>>,
                i32,
                i32,
                Option<String>,
            ),
        >(
            "SELECT u.uid, u.name, a.email, a.parsed_hash, a.pending_action, u.status, u.resets, required_name_change.message
                    FROM public.user u 
                    INNER JOIN user_login a 
                        ON u.uid = a.uid
                    LEFT JOIN required_name_change
                        ON required_name_change.uid = u.uid
                       AND required_name_change.completed_at is null
                WHERE u.uid = $1",
        )
        .bind(uid)
        .fetch_optional(&self.sql_pool)
        .await?
        .ok_or(users::UserError::NotFound)?;

        Ok(User {
            uid,
            username: name,
            email,
            parsed_hash,
            status: status.try_into()?,
            pending_action: match pending_action {
                Some(p) => Some(p.0),
                _ => None,
            },
            reset_count: resets,
            required_name_change_message,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_email(&self, email: &str) -> Result<Vec<users::User>, users::UserError> {
        let users = sqlx::query_as::<
            _,
            (
                String,
                String,
                String,
                String,
                Option<Json<UserPendingAction>>,
                i32,
                i32,
                Option<String>,
            ),
        >(
            "SELECT u.uid, u.name, a.email, a.parsed_hash, a.pending_action, u.status, u.resets, required_name_change.message
                FROM public.user u 
                INNER JOIN user_login a 
                    ON u.uid = a.uid
                LEFT JOIN required_name_change
                    ON required_name_change.uid = u.uid
                   AND required_name_change.completed_at is null
            WHERE a.email = $1",
        )
        .bind(email.trim().to_lowercase())
        .fetch_all(&self.sql_pool)
        .await?;

        let users = users
            .into_iter()
            .map(
                |(
                    uid,
                    name,
                    email,
                    parsed_hash,
                    pending_action,
                    status,
                    resets,
                    required_name_change_message,
                )| {
                    Ok(User {
                        uid,
                        username: name,
                        email,
                        parsed_hash,
                        status: status.try_into()?,
                        pending_action: match pending_action {
                            Some(p) => Some(p.0),
                            _ => None,
                        },
                        reset_count: resets,
                        required_name_change_message,
                    })
                },
            )
            .collect::<Result<Vec<_>, users::UserError>>()?;
        Ok(users)
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_username(&self, username: &str) -> Result<users::User, UserError> {
        // get uid, name, email, parsed_hash, state, and the state from throat.user
        let (uid, name, email, parsed_hash, pending_action, status, resets, required_name_change_message) = sqlx::query_as::<
            _,
            (
                String,
                String,
                String,
                String,
                Option<Json<UserPendingAction>>,
                i32,
                i32,
                Option<String>,
            ),
        >(
            "SELECT u.uid, u.name, a.email, a.parsed_hash, a.pending_action, u.status, u.resets, required_name_change.message
                    FROM public.user u 
                    INNER JOIN user_login a 
                        ON u.uid = a.uid
                    LEFT JOIN username_history uh
                        ON uh.uid = u.uid
                    LEFT JOIN required_name_change
                        ON required_name_change.uid = u.uid
                       AND required_name_change.completed_at is null
                WHERE LOWER(u.name) = LOWER($1)
                   OR LOWER(uh.name) = LOWER($1)",
        )
        .bind(username)
        .fetch_optional(&self.sql_pool)
        .await?
        .ok_or(users::UserError::NotFound)?;

        Ok(User {
            uid,
            username: name,
            email,
            parsed_hash,
            pending_action: match pending_action {
                Some(p) => Some(p.0),
                _ => None,
            },
            status: status.try_into()?,
            reset_count: resets,
            required_name_change_message,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn write_user(
        &self,
        code: &str,
        users::User {
            email,
            uid,
            username,
            parsed_hash,
            pending_action,
            status,
            reset_count,
            required_name_change_message,
        }: &users::User,
    ) -> Result<(), UserError> {
        if !self.is_registration_enabled().await? {
            return Err(UserError::RegistrationDisabled);
        };

        let code_required = self.is_registration_code_required().await?;

        let mut tx = self.sql_pool.begin().await?;

        let status_code = *status as i32;

        if code_required {
            // Update the invite_code table
            let updated_invite_code = sqlx::query(
                r#"
                WITH valid_invite_code AS (
                    SELECT id
                    FROM invite_code
                    LEFT JOIN public.user u
                      ON u.uid = invite_code.uid
                    WHERE code = $1
                    AND uses < max_uses
                    AND (expires IS NULL OR expires > NOW())
                    AND u.status = 0
                    ORDER BY uses ASC, expires ASC
                    LIMIT 1
                )
                UPDATE invite_code
                SET uses = uses + 1
                WHERE id = (SELECT id FROM valid_invite_code)
                RETURNING code
                "#,
            )
            .bind(code)
            .fetch_optional(&mut *tx)
            .await?;

            if updated_invite_code.is_none() {
                return Err(UserError::BadCode);
            }
        }

        // Insert a new row into the public.user table and the user_login table
        let result = sqlx::query(
            r#"
            WITH new_user AS (
                INSERT INTO public.user (uid, name, status, joindate, resets)
                SELECT $1, $2, $3, NOW(), 0
                WHERE NOT EXISTS (SELECT 1 FROM public.user WHERE lower(name) = lower($2))
                  AND NOT EXISTS (SELECT 1 FROM username_history WHERE lower(name) = lower($2))
                RETURNING uid
            )
            INSERT INTO user_login (uid, email, parsed_hash, pending_action)
            SELECT uid, $4, $5, $6 FROM new_user
        "#,
        )
        .bind(uid)
        .bind(username)
        .bind(status_code)
        .bind(email)
        .bind(parsed_hash)
        .bind(pending_action.as_ref().map(Json))
        .execute(&mut *tx)
        .await?;

        if result.rows_affected() == 0 {
            return Err(UserError::Exists);
        }

        // Insert a new row into the sub_subscriber table and update the sub table by incrementing the subscribers count
        sqlx::query(
            r#"
            WITH defaults AS (
                SELECT value AS sid
                FROM site_metadata
                WHERE key = 'default'
            ), insert_sub_subscriber AS (
                INSERT INTO sub_subscriber (uid, sid, status, time)
                SELECT $1, sid, 1, NOW()
                FROM defaults
                RETURNING sid
            )
            UPDATE sub
            SET subscribers = subscribers + 1
            WHERE sub.sid IN (SELECT sid FROM insert_sub_subscriber)
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Insert rows into the user_metadata table
        sqlx::query(
            r#"
            INSERT INTO user_metadata (uid, key, value)
            VALUES
                ($1, 'nsfw', '1'),
                ($1, 'nsfw_blur', '1')
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        if code_required {
            // Insert a new row into the user_metadata table
            sqlx::query(
                r#"
            INSERT INTO user_metadata (uid, key, value)
            VALUES
                ($1, 'invitecode', $2)
            "#,
            )
            .bind(uid)
            .bind(code)
            .execute(&mut *tx)
            .await?;
        }

        tx.commit().await?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn update_user(&self, user: &users::User) -> Result<(), UserError> {
        // Update user_login table and conditionally insert into customer_update_log table
        sqlx::query(
            r#"
                WITH updated_user AS (
                    UPDATE user_login
                    SET email = $1, parsed_hash = $2, pending_action = $3
                    WHERE uid = $4
                    RETURNING email
                ),
                reset_count AS (
                    UPDATE public.user
                    SET resets = $5
                    WHERE uid = $4
                ),
                current_email AS (
                    SELECT email
                    FROM user_login
                    WHERE uid = $4
                )
                INSERT INTO customer_update_log (uid, action, value, created, completed, success)
                SELECT $4, 'change_email', $1, CURRENT_TIMESTAMP, NULL, NULL
                FROM updated_user, current_email
                WHERE updated_user.email != current_email.email
            "#,
        )
        .bind(&user.email)
        .bind(&user.parsed_hash)
        .bind(user.pending_action.as_ref().map(Json))
        .bind(&user.uid)
        .bind(user.reset_count)
        .execute(&self.sql_pool)
        .await?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn change_username(&self, user: &users::User) -> Result<(), UserError> {
        let max_length = self.get_username_max_length().await?;

        if user.username.len() > max_length {
            return Err(users::UserError::InvalidUsername);
        }

        if let Some(banned_substring) = self.find_banned_substring(&user.username).await? {
            return Err(users::UserError::BannedUsername(banned_substring.clone()));
        }

        // Update user_login table and conditionally insert into customer_update_log table
        let result = sqlx::query(
            r#"
               with
                 config as (
                   select (select value::integer
                             from site_metadata
                            where key = 'site.username_change.limit') as change_limit,
                          (select (case
                                   when value = '0' then null
                                   else now() - value::integer * interval '1 day'
                                   end)
                             from site_metadata
                            where key = 'site.username_change.limit_days') as cutoff
                 ),
                 name_in_use as (
                     select uid
                       from public.user
                      where name = $1

                      union

                     select uid
                       from public.user
                      where lower(name) = lower($1)
                        and uid != $2

                      union

                     select uid
                       from username_history
                      where lower(name) = lower($1)
                        and uid != $2

                      union

                     select uid
                       from username_history
                      where name = $1
                        and required_id is not null),
                 recent_history as (
                   select count(*) as count
                     from username_history
                    where uid = $2
                      and ((select cutoff from config) is null
                           or changed > (select cutoff from config))
                 ),
                 requirement as (
                   select id
                     from required_name_change
                    where uid = $2
                      and completed_at is null
                 ),
                 update_user as (
                   update public.user
                      set name = $1
                    where uid = $2
                      and not exists (select uid from name_in_use)
                      and ((select count from recent_history) < (select change_limit from config)
                           or exists (select id from requirement))
                   returning name
                 ),
                 username_history as (
                   insert into username_history (uid, name, changed, required_id)
                   select $2 as uid,
                          u.name,
                          now() as changed,
                          (select id from requirement) as required_id
                     from public.user u
                    where u.uid = $2
                      and exists (select name from update_user)
                 ),
                 update_required_name_change as (
                   update required_name_change
                      set completed_at = now()
                    where id = (select id from requirement)
                      and exists (select name from update_user)
                 )
               select case
                      when ((select count from recent_history) >= (select change_limit from config)
                            and not exists (select id from requirement)) then 'Not permitted'
                      when exists (select uid from name_in_use) then 'Name in use'
                      else 'Success' end as result
             "#,
        )
        .bind(&user.username)
        .bind(&user.uid)
        .fetch_one(&self.sql_pool)
        .await?
        .get::<String, _>(0);

        match result.as_str() {
            "Not permitted" => Err(UserError::InvalidState),
            "Name in use" => Err(UserError::Exists),
            _ => Ok(()),
        }
    }

    #[tracing::instrument(skip(self))]
    async fn soft_delete(&self, uid: &str) -> Result<(), UserError> {
        // create a transaction
        let mut tx = self.sql_pool.begin().await?;

        // remove the user from the user_login table
        sqlx::query(
            r#"
            DELETE FROM user_login
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Insert a new row into the customer_update_log table to cancel the subscription
        sqlx::query(
            r#"
            INSERT INTO customer_update_log (uid, action, created)
            SELECT $1, 'cancel_subscription', NOW()
            WHERE EXISTS (
                SELECT 1
                FROM user_metadata
                WHERE uid = $1 AND key = 'stripe_customer'
            )
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Update the user table
        sqlx::query(
            r#"
            UPDATE public.user
            SET status = 10
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        tx.commit().await?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn delete_user(&self, uid: &str) -> Result<(), UserError> {
        let mut tx = self.sql_pool.begin().await?;

        // Get the code from the user_metadata table
        let code: Option<String> = sqlx::query_scalar(
            r#"
            SELECT value
            FROM user_metadata
            WHERE uid = $1 AND key = 'invitecode'
            "#,
        )
        .bind(uid)
        .fetch_optional(&mut *tx)
        .await?;

        // Delete rows from the user_metadata table
        sqlx::query(
            r#"
            DELETE FROM user_metadata
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Decrement the subscribers count in the sub table
        sqlx::query(
            r#"
            UPDATE sub
            SET subscribers = subscribers - 1
            WHERE sid IN (
                SELECT sid
                FROM sub_subscriber
                WHERE uid = $1
            )
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Delete the row from the sub_subscriber table
        sqlx::query(
            r#"
            DELETE FROM sub_subscriber
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Delete the row from the user_login table
        sqlx::query(
            r#"
            DELETE FROM user_login
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        // Delete the row from the public.user table
        sqlx::query(
            r#"
            DELETE FROM public.user
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut *tx)
        .await?;

        if let Some(code) = code {
            // Decrement the most valid invite_code
            sqlx::query(
                r#"
                WITH valid_invite_code AS (
                    SELECT id
                    FROM invite_code
                    WHERE code = $1
                    AND uses > 0
                    AND (expires IS NULL OR expires > NOW())
                    ORDER BY uses ASC, expires ASC
                    LIMIT 1
                )
                UPDATE invite_code
                SET uses = uses - 1
                WHERE id = (SELECT id FROM valid_invite_code)
                "#,
            )
            .bind(&code)
            .execute(&mut *tx)
            .await?;
        }

        tx.commit().await?;

        Ok(())
    }
}
