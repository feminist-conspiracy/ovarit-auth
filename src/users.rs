use async_trait::async_trait;
use pbkdf2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Pbkdf2,
};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::{fmt::Display, sync::Arc};
use thiserror::Error;

use crate::{authenticate::AuthenticationError, create::NewUser};

#[derive(Error, Debug)]
pub enum UserError {
    #[error("Unable to find user")]
    NotFound,
    #[error("User already exists")]
    Exists,
    #[error("Invalid State")]
    InvalidState,
    #[error("Password Complexity Error")]
    PasswordComplexity,
    #[error("Internal error due to password hasher: {0}")]
    PasswordHashingError(pbkdf2::password_hash::Error),

    #[error("Database error: {0}")]
    DBError(#[from] sqlx::Error),
    #[error("Unable to create user because invite code was invalid")]
    BadCode,
    #[error("Unable to create user because email is banned")]
    BannedEmailDomain,
    #[error("Unable to create user because registration is disabled")]
    RegistrationDisabled,
    #[error("Unable to create user because username is invalid")]
    InvalidUsername,
    #[error("Unable to create user because email is invalid")]
    InvalidEmail,
    #[error("Unable to create user because username is banned: {0}")]
    BannedUsername(String),
}

impl From<pbkdf2::password_hash::Error> for UserError {
    fn from(e: pbkdf2::password_hash::Error) -> Self {
        UserError::PasswordHashingError(e)
    }
}

pub fn serialize_lower<S>(data: &str, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&data.to_lowercase())
}

pub fn deserialize_lower<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    Ok(s.to_lowercase())
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct User {
    pub uid: String,
    pub username: String,
    pub email: String,
    pub parsed_hash: String,
    pub status: UserStatus,
    pub pending_action: Option<UserPendingAction>,
    pub reset_count: i32,
    pub required_name_change_message: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum UserStatus {
    Normal = 0,
    Banned = 5,
    Deleted = 10,
}

impl TryFrom<i32> for UserStatus {
    type Error = UserError;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            x if x == UserStatus::Normal as i32 => Ok(UserStatus::Normal),
            x if x == UserStatus::Banned as i32 => Ok(UserStatus::Banned),
            x if x == UserStatus::Deleted as i32 => Ok(UserStatus::Deleted),
            _ => Err(UserError::InvalidState),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum UserPendingAction {
    Verify { delete: String, code: String },
    NewEmail { email: String, code: String },
    Reset { code: String, time: u64 },
}

#[async_trait]
pub trait DataStore {
    /// Returns a user given the users email.
    async fn get_user_by_email(&self, email: &str) -> Result<Vec<User>, UserError>;
    /// Returns a user given the users username.
    async fn get_user_by_username(&self, username: &str) -> Result<User, UserError>;
    /// Returns a user given the users uid.
    async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError>;
    /// Returns a user by a given code.
    async fn get_user_by_code(&self, code: &str) -> Result<User, UserError>;

    /// Writes a user only if the user doesn't already exist in the system. If the user does exist throws an error.
    async fn write_user(&self, code: &str, user: &User) -> Result<(), UserError>;
    /// Updates a user provided that the user already exists.
    async fn update_user(&self, user: &User) -> Result<(), UserError>;
    /// Updates a user's name provided that the user already exists.
    async fn change_username(&self, user: &User) -> Result<(), UserError>;

    /// Deletes a user provided that the user already exists.
    async fn delete_user(&self, uid: &str) -> Result<(), UserError>;

    /// Soft deletes a user, removing them from the login process.
    async fn soft_delete(&self, uid: &str) -> Result<(), UserError>;

    /// Validates a NewUser object and returns a User object if the NewUser object is valid.
    async fn validate_new_user(&self, new_user: NewUser) -> Result<User, UserError>;
}

pub mod in_memory {
    use crate::create::NewUser;
    use crate::users::{
        hash_password, ContactType, DataStore, EmailError, User, UserContact, UserError,
        UserPendingAction, UserStatus,
    };
    use async_trait::async_trait;
    use std::collections::HashMap;
    use std::sync::Arc;
    use tokio::sync::RwLock;
    use uuid::Uuid;

    #[derive(Default)]
    pub struct InMemoryDataStore {
        users: Arc<RwLock<HashMap<String, User>>>,
    }

    #[async_trait]
    impl DataStore for InMemoryDataStore {
        async fn validate_new_user(&self, new_user: NewUser) -> Result<User, UserError> {
            let users = self.users.read().await;

            if users
                .values()
                .any(|user| user.username.to_lowercase() == new_user.username.to_lowercase())
            {
                return Err(UserError::Exists);
            }

            if new_user.username.len() > 32 {
                return Err(UserError::InvalidUsername);
            }

            let delete_code = Uuid::new_v4().as_hyphenated().to_string();
            let code = Uuid::new_v4().as_hyphenated().to_string();
            let user = User {
                uid: Uuid::new_v4().as_hyphenated().to_string(),
                username: new_user.username,
                email: new_user.email,
                parsed_hash: hash_password(&new_user.password)?,
                status: UserStatus::Normal,
                pending_action: Some(UserPendingAction::Verify {
                    delete: delete_code.clone(),
                    code: code.clone(),
                }),
                reset_count: 0,
                required_name_change_message: None,
            };
            Ok(user)
        }

        async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError> {
            let users = self.users.read().await;
            users.get(uid).cloned().ok_or(UserError::NotFound)
        }

        async fn get_user_by_code(&self, code: &str) -> Result<User, UserError> {
            let users = self.users.read().await;
            users
                .values()
                .find(|user| {
                    if let Some(pending_action) = &user.pending_action {
                        match pending_action {
                            UserPendingAction::Verify { code: c, .. } => c == code,
                            UserPendingAction::NewEmail { code: c, .. } => c == code,
                            UserPendingAction::Reset { code: c, .. } => c == code,
                        }
                    } else {
                        false
                    }
                })
                .cloned()
                .ok_or(UserError::NotFound)
        }

        async fn get_user_by_email(&self, email: &str) -> Result<Vec<User>, UserError> {
            let users = self.users.read().await;
            let result: Vec<User> = users
                .values()
                .filter(|user| user.email == email)
                .cloned()
                .collect();
            Ok(result)
        }

        async fn get_user_by_username(&self, username: &str) -> Result<User, UserError> {
            let users = self.users.read().await;
            users
                .values()
                .find(|user| user.username.to_lowercase() == username.to_lowercase())
                .cloned()
                .ok_or(UserError::NotFound)
        }

        async fn write_user(&self, _code: &str, user: &User) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if users.contains_key(&user.uid)
                || users
                    .iter()
                    .any(|(_, u)| u.username.to_lowercase() == user.username.to_lowercase())
            {
                return Err(UserError::Exists);
            }
            users.insert(user.uid.clone(), user.clone());
            Ok(())
        }

        async fn update_user(&self, user: &User) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if let Some(existing_user) = users.get_mut(&user.uid) {
                *existing_user = user.clone();
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }

        async fn change_username(&self, user: &User) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if let Some(existing_user) = users.get_mut(&user.uid) {
                *existing_user = user.clone();
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }

        async fn soft_delete(&self, uid: &str) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if let Some(existing_user) = users.get_mut(uid) {
                existing_user.status = UserStatus::Deleted;
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }

        async fn delete_user(&self, uid: &str) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if users.remove(uid).is_some() {
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }
    }

    // Your existing ContactType and UserContact trait definitions...

    pub struct StdOutUserContact;

    #[async_trait]
    impl UserContact for StdOutUserContact {
        async fn send_email(
            &self,
            ty: ContactType<'_>,
            email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), EmailError> {
            let message = match ty {
                ContactType::Verify => "Please verify your email address",
                ContactType::Reset(_) => "Please reset your password",
                ContactType::Update => "Please update your email address",
            };

            println!("Sending email to: {}", email);
            println!("UID: {}", uid);
            println!("Message: {}", message);
            println!("Code: {}", code);
            match ty {
                ContactType::Verify => {
                    println!("Url: http://localhost:3000/register/confirm?code={code}")
                }
                ContactType::Update => {
                    println!(
                        "Url: http://localhost:3000/settings/account/confirm_email?code={code}"
                    )
                }
                ContactType::Reset(name) => {
                    println!("Url: http://localhost:3000/reset/complete?code={code}&name={name}")
                }
            }
            println!("-------------------------");

            Ok(())
        }
    }
}

#[derive(Error, Debug)]
pub enum EmailError {
    #[error("Unable to contact mail server")]
    MailServerOffline,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum ContactType<'a> {
    Verify,
    Reset(&'a str),
    Update,
}

#[async_trait]
pub trait UserContact {
    async fn send_email(
        &self,
        ty: ContactType<'_>,
        email: &str,
        uid: &str,
        code: &str,
    ) -> Result<(), EmailError>;
}

pub fn hash_password(password: &str) -> Result<String, UserError> {
    if cfg!(feature = "weak_hashing") {
        println!("Using weak_hashing!");
        return Ok(password.to_string());
    }
    Ok(Pbkdf2
        .hash_password(password.as_bytes(), &SaltString::generate(&mut OsRng))?
        .to_string())
}

#[tracing::instrument(skip(password, hash))]
pub async fn verify_password(password: &str, hash: &str) -> Result<(), AuthenticationError> {
    let password = password.to_string();
    let hash = hash.to_string();

    if cfg!(feature = "weak_hashing") {
        println!("Using weak_hashing!");
        return if password == hash {
            Ok(())
        } else {
            Err(AuthenticationError::BadPassword)
        };
    }

    tokio::task::spawn_blocking(move || {
        let parsed_hash = PasswordHash::new(&hash)?;
        Pbkdf2
            .verify_password(password.as_bytes(), &parsed_hash)
            .map_err(|_| AuthenticationError::BadPassword)
    })
    .await
    .expect("This should only happen on cancel/panic")
}

use tracing::error;

#[derive(Error, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct UserInfo {
    pub username: String,
    pub email: String,
}

impl Display for UserInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}", self.username, self.email)
    }
}

#[tracing::instrument(skip(user_store))]
pub async fn info(
    user_id: &str,
    user_store: &Arc<dyn DataStore + Send + Sync>,
) -> Result<UserInfo, UserError> {
    let user = user_store.get_user_by_uid(user_id).await?;
    Ok(UserInfo {
        username: user.username,
        email: user.email,
    })
}

pub fn is_valid_username(username: &str) -> bool {
    println!("Valid? {}", username);
    if username.len() < 3 || username.len() > 64 {
        return false;
    }
    username
        .chars()
        .all(|c| c.is_ascii_alphanumeric() || c == '_' || c == '-')
}
