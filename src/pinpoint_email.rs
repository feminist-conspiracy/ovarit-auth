use async_trait::async_trait;
use aws_sdk_pinpoint::types::{
    AddressConfiguration, ChannelType, MessageRequest, Template, TemplateConfiguration,
};
use ovarit_auth::users::{self, ContactType};
use tracing::error;

#[derive(Clone)]
pub struct PinpointContact {
    pub client: aws_sdk_pinpoint::Client,
}

#[async_trait]
impl users::UserContact for PinpointContact {
    #[tracing::instrument(skip(self))]
    async fn send_email(
        &self,
        ty: users::ContactType<'_>,
        email: &str,
        uid: &str,
        code: &str,
    ) -> Result<(), users::EmailError> {
        self.client
            .send_messages()
            .application_id("1881c5e2626148f691b90d7dba50df25")
            .message_request(
                MessageRequest::builder()
                    .addresses(
                        email,
                        AddressConfiguration::builder()
                            .channel_type(ChannelType::Email)
                            .set_substitutions(Some(
                                [
                                    Some(("uid".to_string(), vec![uid.to_string()])),
                                    Some(("code".to_string(), vec![code.to_string()])),
                                    if let ContactType::Reset(s) = ty {
                                        Some(("name".to_string(), vec![s.to_string()]))
                                    } else {
                                        None
                                    },
                                ]
                                .into_iter()
                                .flatten()
                                .collect(),
                            ))
                            .build(),
                    )
                    .set_template_configuration(Some(
                        TemplateConfiguration::builder()
                            .set_email_template(match ty {
                                ContactType::Verify => {
                                    Some(Template::builder().name("verify-email").build())
                                }
                                ContactType::Reset(_) => {
                                    Some(Template::builder().name("reset-password").build())
                                }
                                ContactType::Update => {
                                    Some(Template::builder().name("update-email").build())
                                }
                            })
                            .build(),
                    ))
                    .build(),
            )
            .send()
            .await
            .map_err(|e| {
                error!("Failed to send email: {:?}", e);
                users::EmailError::MailServerOffline
            })?;

        Ok(())
    }
}
