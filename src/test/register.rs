use super::*;

// A user can log in if they register correctly.
// Usernames are case-insensitive.
#[tokio::test]
async fn test_register_to_login() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let name = user.username + "Xy";

    register_and_verify(&mut auth, &name, &user.password, &user.email).await;

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": name,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    assert!(response.headers().get(SET_COOKIE).is_some());

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": name.to_uppercase(),
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    assert!(response.headers().get(SET_COOKIE).is_some());

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": name.to_lowercase(),
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    assert!(response.headers().get(SET_COOKIE).is_some());
}

// The verification link must be correct.
#[tokio::test]
async fn test_register_try_bad_verification_link() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::CREATED);
    let msgs = get_email_msgs!(&user.email);
    let Email { uid, .. } = &msgs[msgs.len() - 1];

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": "bad_code",
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::NOT_FOUND);
    assert!(response.headers().get(SET_COOKIE).is_none());

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);
}

// Trying to register without a valid email will fail.
#[tokio::test]
async fn test_register_requires_valid_email() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    const INVALID_EMAILS: [&str; 7] = [
        "",
        " ",
        "typo",
        "typo#example.com",
        "typo@example,com",
        "typo@",
        "@example.com",
    ];

    for addr in INVALID_EMAILS.iter() {
        let user = User::random();
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": user.username,
            "password": user.password,
            "email": addr.to_string(),
            "invite_code": "default_code",
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST, "{}", addr);

        let msg_count = match get_emails().await.lock().unwrap().get(&addr.to_string()) {
            Some(emails) => emails.len(),
            None => 0,
        };
        assert_eq!(msg_count, 0);

        let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
            "username": user.username,
            "password": user.password,
        });
        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }
}

// Trying to register a user with the same name as an existing one fails.
// The username comparison should be case-insensitive.
#[tokio::test]
async fn test_cannot_register_name_already_in_use() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let username = user.username.to_string() + "Xx";
    register_and_verify(&mut auth, &username, &user.password, &user.email).await;

    let user2 = User::random();
    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": username,
        "password": user2.password,
        "email": user2.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": username.to_uppercase(),
        "password": user2.password,
        "email": user2.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    let user2 = User::random();
    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": username.to_lowercase(),
        "password": user2.password,
        "email": user2.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::BAD_REQUEST);
}

// Username content rules are enforced.
#[tokio::test]
async fn test_username_validation() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);

    let invalid_names: [&str; 6] = [
        "",
        "        ",
        "a",
        // site.username_max_length defaults to 32.
        // The database field limit is 64.
        &random_string(64),
        "invalid!character",
        "white space",
    ];

    for name in invalid_names.iter() {
        let user = User::random();
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": name,
            "password": user.password,
            "email": user.email,
            "invite_code": "default_code",
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST, "{}", name);
    }
}
