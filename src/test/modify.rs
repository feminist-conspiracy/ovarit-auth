use axum::body::to_bytes;

use super::*;

// A user can reset their password using a link sent to their email.
#[tokio::test]
async fn test_reset_password() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_password = random_string(10) + "Bb1!";

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": user.email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The email on file gets a reset password message.
    let msgs = get_email_msgs!(&user.email);
    let Email { code, ty, uid, .. } = &msgs[msgs.len() - 1];
    assert!(matches!(ty, &ContactType::Reset(_)));
    if let ContactType::Reset(username) = ty {
        assert_eq!(username, &user.username);
    }

    // Log in with the old password and get a cookie.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let orig_session = get_cookie_value(response.headers(), "session");

    // Now complete the reset process.
    let response = make_request!(auth, "POST", "/ovarit_auth/reset/complete", {
        "uid": uid,
        "code": code,
        "new_password": new_password,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The new password can be used to log in.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": new_password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // The new session cookie works.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    assert_eq!(response.status(), StatusCode::OK);

    // The old session cookie is now invalid.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", orig_session, {});
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // The reset code only works once.
    let password = random_string(10) + "Xyz1~";
    let response = make_request!(auth, "POST", "/ovarit_auth/reset/complete", {
        "uid": uid,
        "code": code,
        "new_password": password,
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);
}

// A user can change their password, using the old password.
#[tokio::test]
async fn test_change_password() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_password = random_string(10) + "Cc1!";

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    // Log in with the old password and get a cookie.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // Try to change password, using an incorrect password.
    let response = make_request_with_session!(
    auth,
    "POST",
    "/ovarit_auth/update",
    session,
    {
        "password": "not_the_password",
        "new_password": new_password,
        "new_email": null,
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);

    // Request the change, using the correct password.
    let response = make_request_with_session!(
    auth,
    "POST",
    "/ovarit_auth/update",
    session,
    {
        "password": user.password,
        "new_password": new_password,
        "new_email": null,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // the session cookie from that update request should be valid
    let session2 = get_cookie_value(response.headers(), "session");

    // The old session should now be invalid.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // The new session cookie should be valid.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session2, {});
    assert_eq!(response.status(), StatusCode::OK);

    // Log in with the new password.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": new_password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);

    // Verify the old email still works.
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": user.email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The email on file gets a reset password message.
    let msgs = get_email_msgs!(&user.email);
    let Email { ty, .. } = &msgs[msgs.len() - 1];
    assert!(matches!(ty, &ContactType::Reset(_)));
    if let ContactType::Reset(username) = ty {
        assert_eq!(username, &user.username);
    }
}

// A user can change their email, using their password.
#[tokio::test]
async fn test_change_email() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_email = format!("{}@{}", random_string(5), "example.org");

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    // Log in and get a cookie.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // Change the email address.
    let response = make_request_with_session!(
        auth,
        "POST",
        "/ovarit_auth/update",
        session, {
            "password": user.password,
            "new_password": null,
            "new_email": new_email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The user can still log in while the change is pending.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);

    // Get the message sent to the new email.
    let msgs = get_email_msgs!(&new_email);
    let Email { code, ty, uid, .. } = &msgs[msgs.len() - 1];
    assert_eq!(ty, &ContactType::Update);

    // Confirm the new email.
    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);

    // The old session cookie is not invalidated by the email change.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    assert_eq!(response.status(), StatusCode::OK);

    // The new email is used for password reset links.
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": new_email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The new email receives the reset password message.
    let msgs = get_email_msgs!(&new_email);
    let Email { ty, .. } = &msgs[msgs.len() - 1];
    assert!(matches!(ty, &ContactType::Reset(_)));
    if let ContactType::Reset(username) = ty {
        assert_eq!(username, &user.username);
    }
}

// A user can change their email and fix their mistakes.
#[tokio::test]
async fn test_change_email_twice() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_email1 = format!("{}@{}", random_string(5), "example.org");
    let new_email2 = format!("{}@{}", random_string(5), "example.org");

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    // Log in and get a cookie.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // Change the email address.
    let response = make_request_with_session!(
        auth,
        "POST",
        "/ovarit_auth/update",
        session, {
            "password": user.password,
            "new_password": null,
            "new_email": new_email1,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // Change the email address again. Mistakes happen.
    let response = make_request_with_session!(
        auth,
        "POST",
        "/ovarit_auth/update",
        session, {
            "password": user.password,
            "new_password": null,
            "new_email": new_email2,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // Get the message sent to the first new email.
    let msgs = get_email_msgs!(&new_email1);
    let Email { code, ty, uid, .. } = &msgs[msgs.len() - 1];
    assert_eq!(ty, &ContactType::Update);

    // The first code should no longer work.
    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::NOT_FOUND);

    // Get the message sent to the second new email.
    let msgs = get_email_msgs!(&new_email2);
    let Email { code, ty, uid, .. } = &msgs[msgs.len() - 1];
    assert_eq!(ty, &ContactType::Update);

    // Confirm the change with the second code.
    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);

    // The desired email is used for password reset links.
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": new_email2,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The confirmed email receives the reset password message.
    let msgs = get_email_msgs!(&new_email2);
    let Email { ty, .. } = &msgs[msgs.len() - 1];
    assert!(matches!(ty, &ContactType::Reset(_)));
    if let ContactType::Reset(username) = ty {
        assert_eq!(username, &user.username);
    }
}

// A user can make mistakes and recover.
#[tokio::test]
async fn test_reset_with_unconfirmed_email() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_email = format!("{}@{}", random_string(5), "example.org");

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    // Log in and get a cookie.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // Request the email address change.
    let response = make_request_with_session!(
        auth,
        "POST",
        "/ovarit_auth/update",
        session, {
            "password": user.password,
            "new_password": null,
            "new_email": new_email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // Request a password reset.
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": user.email,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The old email receives the reset password message.
    let msgs = get_email_msgs!(&user.email);
    let Email { ty, .. } = &msgs[msgs.len() - 1];
    assert!(matches!(ty, &ContactType::Reset(_)));
    if let ContactType::Reset(username) = ty {
        assert_eq!(username, &user.username);
    }
}

// Users can fetch their email addresses using a session cookie.
#[tokio::test]
async fn test_info() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
    let json_str = String::from_utf8(bytes.into()).unwrap();
    let val: Value = serde_json::from_str(&json_str).unwrap();
    assert_eq!(val["email"], user.email);
}

// Users can change their usernames, within configured limits.
#[tokio::test]
async fn test_change_username_limits() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_username1 = random_string(5);
    let new_username2 = random_string(5);

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();

        // Disable all name changes.
        sqlx::query(
            r#"
            update site_metadata
               set value = '0'
             where key = 'site.username_change.limit'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        // Attempt a name change.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": user.username,
            "password": user.password,
            "new_username": new_username1,
        });
        assert_eq!(response.status(), StatusCode::FORBIDDEN);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(
            val["reason"],
            "You are not allowed to change your username at this time."
        );

        // Enable name changes and set a rate limit
        sqlx::query(
            r#"
            update site_metadata
               set value = '1'
             where key = 'site.username_change.limit'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        sqlx::query(
            r#"
            update site_metadata
               set value = '1'
             where key = 'site.username_change.limit_days'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();
    }

    // Make a name change.
    let response = make_request!(
    auth,
    "POST",
    "/ovarit_auth/change_username",
    {
        "username": user.username,
        "password": user.password,
        "new_username": new_username1,
    });
    assert_eq!(response.status(), StatusCode::OK);

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();

        // A second name change hits the limit.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": new_username1,
            "password": user.password,
            "new_username": new_username2,
        });
        assert_eq!(response.status(), StatusCode::FORBIDDEN);

        // The limit only applies within the configured number of days.
        sqlx::query(
            r#"
            update username_history
               set changed = changed - interval '2 days'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        // With the first name change now recorded as 2 days ago, the
        // second one succeeds.  Reverting to an old name is allowed.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": new_username1,
            "password": user.password,
            "new_username": user.username,
        });
        assert_eq!(response.status(), StatusCode::OK);

        // Setting limit_days to 0 applies to all changes over time.
        sqlx::query(
            r#"
            update site_metadata
               set value = '0'
             where key = 'site.username_change.limit_days'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": new_username1,
            "password": user.password,
            "new_username": new_username2,
        });
        assert_eq!(response.status(), StatusCode::FORBIDDEN);
    }
}

// User must be logged in and password must be correct to change username.
#[tokio::test]
async fn test_change_username_auth() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let new_username = random_string(5);

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    // Username change with incorrect password fails.
    let response = make_request!(
    auth,
    "POST",
    "/ovarit_auth/change_username",
    {
        "username": user.username,
        "password": "password",
        "new_username": new_username,
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);
    let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
    let json_str = String::from_utf8(bytes.into()).unwrap();
    let val: Value = serde_json::from_str(&json_str).unwrap();
    assert_eq!(val["reason"], "Incorrect password");
}

// User name change requires a valid username.
#[tokio::test]
async fn test_change_username_validity() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let user2 = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    register_and_verify(&mut auth, &user2.username, &user2.password, &user2.email).await;

    // Attempt name changes with invalid names.
    let invalid_names: [&str; 5] = ["", "        ", "a", "invalid!character", "white space"];

    for name in invalid_names.iter() {
        let response = make_request!(auth, "POST", "/ovarit_auth/change_username", {
            "username": user.username,
            "new_username": name,
            "password": user.password,
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST, "{}", name);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(val["reason"], "Invalid username.", "{}", name);
    }

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();

        // Default username max length in the database is 32.
        let response = make_request!(auth, "POST", "/ovarit_auth/change_username", {
            "username": user.username,
            "new_username": &random_string(33),
            "password": user.password,
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(val["reason"], "Invalid username.");

        sqlx::query(
            r#"
            update site_metadata
               set value = '1'
             where key = 'site.username_change.limit'
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        // Changing to another user's username fails.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": user.username,
            "password": user.password,
            "new_username": user2.username,
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(val["reason"], "Username is not available.");

        // Duplicate username check is case-insensitive
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": user.username,
            "password": user.password,
            "new_username": user2.username.to_uppercase(),
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(val["reason"], "Username is not available.");

        // Add a username history to user2.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": user2.username,
            "password": user2.password,
            "new_username": random_string(5),
        });
        assert_eq!(response.status(), StatusCode::OK);

        // Changing to a name in another user's history fails.
        let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/change_username",
        {
            "username": user.username,
            "password": user.password,
            "new_username": user2.username,
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
        let json_str = String::from_utf8(bytes.into()).unwrap();
        let val: Value = serde_json::from_str(&json_str).unwrap();
        assert_eq!(val["reason"], "Username is not available.");
    }
}
