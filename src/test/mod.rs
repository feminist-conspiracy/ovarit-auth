use std::{collections::HashMap, sync::Mutex};
use std::{env, io};

use async_trait::async_trait;
use axum::http;
use http::{header::SET_COOKIE, StatusCode};
use hyper::Request;
use ovarit_auth::flask_session::decode_and_check;
use ovarit_auth::users::{ContactType, EmailError, UserContact};
use serde_json::json;
use serde_json::Value;
use tokio::sync::OnceCell;
use tower::Service;
use tower_cookies::Cookie;
use tracing::metadata::LevelFilter;
use tracing_log::LogTracer;
use tracing_subscriber::fmt::layer;

use super::*;

#[derive(Debug, Clone)]
struct Email {
    ty: ContactType<'static>,
    uid: String,
    code: String,
}

static EMAILS: OnceCell<Mutex<HashMap<String, Vec<Email>>>> = OnceCell::const_new();
async fn get_emails() -> &'static Mutex<HashMap<String, Vec<Email>>> {
    EMAILS
        .get_or_init(|| async { Mutex::new(HashMap::new()) })
        .await
}

struct InMemUserContact {}

#[async_trait]
impl UserContact for InMemUserContact {
    async fn send_email(
        &self,
        ty: ContactType<'_>,
        email: &str,
        uid: &str,
        code: &str,
    ) -> Result<(), EmailError> {
        let mut emails = get_emails().await.lock().unwrap();
        let email_list = emails.entry(email.to_string()).or_insert(Vec::new());
        let ty_static = match ty {
            ContactType::Reset(s) => ContactType::Reset(Box::leak(s.to_string().into_boxed_str())),
            ContactType::Update => ContactType::Update,
            ContactType::Verify => ContactType::Verify,
        };

        email_list.push(Email {
            ty: ty_static,
            uid: uid.to_string(),
            code: code.to_string(),
        });
        Ok(())
    }
}

static ONLY_ONCE: OnceCell<()> = OnceCell::const_new();

async fn get_app_state() -> AppState {
    ONLY_ONCE
        .get_or_init(|| async {
            // TODO: Audit that the environment access only happens in single-threaded code.
            unsafe { env::set_var("APP_SECRET_KEY", "test") };
            LogTracer::init().unwrap();
            let subscriber = Registry::default()
                .with(tracing_subscriber::Layer::with_filter(
                    layer().pretty().with_writer(io::stdout),
                    LevelFilter::DEBUG,
                ))
                .with(ErrorLayer::default());

            // Set the global tracing subscriber.
            tracing::subscriber::set_global_default(subscriber).unwrap();

            if cfg!(feature = "integration") {
                let pool = create_pool().unwrap();

                // We are going to assume the database is empty. but migrated.
                // We need to create a admin user.
                sqlx::query(
                    r#"
                        INSERT INTO public.user (uid, name, status, joindate)
                        SELECT $1, $2, $3, NOW()
                        WHERE NOT EXISTS (SELECT 1 FROM public.user WHERE name = $2)
                        "#,
                )
                .bind("admin")
                .bind("admin")
                .bind(0)
                .execute(&pool)
                .await
                .unwrap();

                sqlx::query(
                    r#"
                        UPDATE site_metadata
                        SET value = '1'
                        WHERE key = 'site.require_invite_code'
                        "#,
                )
                .execute(&pool)
                .await
                .unwrap();

                sqlx::query(
                    r#"
                        INSERT INTO invite_code (uid, code, uses, max_uses, created)
                        VALUES ('admin', 'default_code', 0, 1000, now())
                        "#,
                )
                .execute(&pool)
                .await
                .unwrap();
            };
        })
        .await;

    if cfg!(feature = "integration") {
        AppState {
            auth_state: AuthState {
                user_store: Arc::new(PostgresUserStore {
                    sql_pool: create_pool().unwrap(),
                }),
                user_contact: Arc::new(InMemUserContact {}),
            },
            post_state: PostEndpointsState {
                // TODO! This should be the real thing.
                task_management: Arc::new(InMemoryTaskManagement::default()),
                file_management: Arc::new(OnDiskFileManagement::default()),
            },
        }
    } else {
        AppState {
            auth_state: AuthState {
                user_store: Arc::new(in_memory::InMemoryDataStore::default()),
                user_contact: Arc::new(InMemUserContact {}),
            },
            post_state: PostEndpointsState {
                task_management: Arc::new(InMemoryTaskManagement::default()),
                file_management: Arc::new(OnDiskFileManagement::default()),
            },
        }
    }
}

/*
Tests that would make Moodle feel better:

-- The delete link does not work after the account is verified.
-- The delete link must be given a valid code.

- A user can change their password and log in with the new password.
- The user can change their password recovery email.
- Changing the password recovery email requires the correct password.
- A user can reset their password using a link sent to their email.
- A user can delete their account.
- A user account which is unconfirmed after two days can be re-registered.
*/

macro_rules! make_request {
    ($auth:expr, $method:expr, $url:expr, $($json:tt)+) => {
        tower::ServiceExt::<Request<Body>>::ready(&mut $auth)
            .await
            .unwrap()
            .call(Request::builder()
                  .method($method)
                  .uri($url)
                  .header(http::header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
                  .body(Body::from(
                      json!($($json)+)
                          .to_string()))
                  .unwrap())
            .await
            .unwrap()
    }
}

macro_rules! make_request_with_session {
    ($auth:expr, $method:expr, $url:expr, $session:expr, $($json:tt)+) => {
        tower::ServiceExt::<Request<Body>>::ready(&mut $auth)
            .await
            .unwrap()
            .call(Request::builder()
                  .method($method)
                  .uri($url)
                  .header(http::header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
                  .header(http::header::COOKIE, format!("session={}", $session))
                  .body(Body::from(
                      json!($($json)+)
                          .to_string()))
                  .unwrap())
            .await
            .unwrap()
    }
}

macro_rules! get_email_msgs {
    ($email:expr) => {
        get_emails()
            .await
            .lock()
            .unwrap()
            .get($email)
            .unwrap()
            .clone()
    };
}

use rand::{distr::Alphanumeric, rng, Rng};

fn random_string(length: usize) -> String {
    rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect()
}

struct User {
    username: String,
    password: String,
    email: String,
}

impl User {
    fn random() -> User {
        User {
            username: random_string(10),
            password: User::random_password(),
            email: format!("{}@example.com", random_string(5).to_lowercase()),
        }
    }

    fn random_password() -> String {
        random_string(10) + "Aa1!"
    }
}

/// Register a user, use the verify email link, and assert that it works.
async fn register_and_verify(mut auth: &mut Router, username: &str, password: &str, email: &str) {
    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": username,
        "password": password,
        "email": email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::CREATED);

    let msgs = get_email_msgs!(email);
    let Email { code, ty, uid, .. } = &msgs[msgs.len() - 1];
    assert_eq!(ty, &ContactType::Verify);

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);
}

/// Get the value for a cookie by name from the Set-Cookie header value.
fn get_cookie_value(headers: &header::HeaderMap, cookie_name: &str) -> String {
    for header_value in headers.get_all(SET_COOKIE).iter() {
        if let Ok(header_str) = header_value.to_str() {
            for cookie_str in header_str.split(',') {
                let cookie = Cookie::parse(cookie_str).unwrap();
                if cookie.name() == cookie_name {
                    return cookie.value().to_string();
                }
            }
        }
    }
    panic!("{} cookie not found", cookie_name);
}

mod delete;
mod email;
mod login;
mod modify;
mod register;
