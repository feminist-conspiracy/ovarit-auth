use axum::body::to_bytes;

use super::*;

// An unverified user account can be re-registered with a new email.
#[tokio::test]
async fn test_delete_and_reregister() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let user2 = User::random();

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
                INSERT INTO invite_code (uid, code, uses, max_uses, created)
                VALUES ('admin', 'reregister', 0, 1, now())
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();
    }
    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "reregister",
    });

    assert_eq!(response.status(), StatusCode::CREATED);
    let bytes = to_bytes(response.into_body(), 1024 * 1024).await.unwrap();
    let json_str = String::from_utf8(bytes.into()).unwrap();
    let val: Value = serde_json::from_str(&json_str).unwrap();
    let response = make_request!(auth, "POST", "/ovarit_auth/delete", {
        "code": val["code"],
        "uid": val["uid"],
    });
    assert_eq!(response.status(), StatusCode::OK);

    // After you do this, the old email link does not work.
    let msgs = get_email_msgs!(&user.email);
    let Email { code, uid, .. } = &msgs[msgs.len() - 1];
    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::NOT_FOUND);

    // Reregister with a different email.
    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user2.password,
        "email": user2.email,
        "invite_code": "reregister",
    });
    assert_eq!(response.status(), StatusCode::CREATED);

    // Confirm the new email.
    let msgs = get_email_msgs!(&user2.email);
    let Email { code, uid, .. } = &msgs[msgs.len() - 1];

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);

    // Login works.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user2.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    assert!(response.headers().get(SET_COOKIE).is_some());
}

// The delete code is checked.
#[tokio::test]
async fn test_delete_requires_valid_code() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "default_code",
    });

    assert_eq!(response.status(), StatusCode::CREATED);
    let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
    let json_str = String::from_utf8(bytes.into()).unwrap();
    let val: Value = serde_json::from_str(&json_str).unwrap();

    // Try to delete and fail.
    let response = make_request!(auth, "POST", "/ovarit_auth/delete", {
        "code": "not the code",
        "uid": val["uid"],
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);

    // Account still valid.
    let msgs = get_email_msgs!(&user.email);
    let Email { code, uid, .. } = &msgs[msgs.len() - 1];

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);
}

// The delete link cannot be used after verification.
#[tokio::test]
async fn test_verified_users_cannot_delete() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "default_code",
    });

    assert_eq!(response.status(), StatusCode::CREATED);
    let bytes = to_bytes(response.into_body(), usize::MAX).await.unwrap();
    let json_str = String::from_utf8(bytes.into()).unwrap();
    let val: Value = serde_json::from_str(&json_str).unwrap();

    let msgs = get_email_msgs!(&user.email);
    assert_eq!(msgs.len(), 1);
    let Email { code, uid, .. } = &msgs[0];

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);

    let response = make_request!(auth, "POST", "/ovarit_auth/delete", {
        "code": val["code"],
        "uid": val["uid"],
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);
}

// An email can be used to register multiple accounts.
#[tokio::test]
async fn test_can_register_multiple_accounts_with_single_email() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let user2 = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    register_and_verify(&mut auth, &user2.username, &user2.password, &user.email).await;

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), http::StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");
    let uid = decode_and_check(session.as_bytes(), 60 * 60 * 24 * 30)
        .unwrap()
        ._user_id;
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user2.username,
        "password": user2.password,
    });
    assert_eq!(response.status(), http::StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");
    let uid2 = decode_and_check(session.as_bytes(), 60 * 60 * 24 * 30)
        .unwrap()
        ._user_id;
    assert_ne!(uid, uid2);
}

// An account cannot be registered using an email with a banned domain.
#[tokio::test]
async fn test_register_with_banned_email_domain() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
                INSERT INTO site_metadata (key, value)
                VALUES ('banned_email_domain', 'banned.com'),
                       ('banned_email_domain', 'spam.com')
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        const INVALID_DOMAINS: [&str; 4] = [
            "banned.com",
            "BANNED.COM",
            "banned.com   ",
            // python app didn't block subdomains
            "spam.spam.com",
        ];
        for domain in INVALID_DOMAINS.iter() {
            let email = format!("{}@{}", random_string(5), domain);
            let response = make_request!(auth, "POST", "/ovarit_auth/create", {
                "username": user.username,
                "password": user.password,
                "email": email,
                "invite_code": "default_code",
            });
            assert_eq!(response.status(), StatusCode::BAD_REQUEST, "{}", email);
        }

        const VALID_DOMAINS: [&str; 3] =
            ["notbanned.com", "NoTbAnNeD.com", "greeneggsandspam.com "];
        for domain in VALID_DOMAINS.iter() {
            let email = format!("{}@{}", random_string(5), domain);
            let response = make_request!(auth, "POST", "/ovarit_auth/create", {
                "username": random_string(5),
                "password": user.password,
                "email": email,
                "invite_code": "default_code",
            });
            assert_eq!(response.status(), StatusCode::CREATED, "{}", email);
        }
    }
}

// A username containing a banned string cannot be registered.
#[tokio::test]
async fn test_register_with_banned_username_string() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
                INSERT INTO site_metadata (key, value)
                VALUES ('banned_username_string', 'bannedstring'),
                       ('banned_username_string', 'anotherone')
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        const INVALID_NAMES: [&str; 2] = ["bannedstringuser", "NotAnotherOne"];
        for name in INVALID_NAMES.iter() {
            let response = make_request!(auth, "POST", "/ovarit_auth/create", {
                "username": name,
                "password": user.password,
                "email": user.email,
                "invite_code": "default_code",
            });

            assert_eq!(response.status(), StatusCode::BAD_REQUEST, "{}", name);
        }
    }
}

// Invalid invite codes cannot be used in registration.
#[tokio::test]
async fn test_register_with_invalid_invite_code() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let user2 = User::random();

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
                INSERT INTO invite_code (uid, code, uses, max_uses, created, expires)
                VALUES ('admin', 'used-up', 1, 1, now(), NULL),
                       ('admin', 'almost-used-up', 9, 10, now(), now() + interval '1 day'),
                       ('admin', 'expired', 0, 1, now(), now() - interval '1 day')
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        // use up 'almost-used-up'
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": user2.username,
            "password": user2.password,
            "email": user2.email,
            "invite_code": "almost-used-up",
        });
        assert_eq!(response.status(), StatusCode::CREATED);

        const INVALID_CODES: [&str; 4] = ["never-created", "used-up", "almost-used-up", "expired"];
        for code in INVALID_CODES.iter() {
            let response = make_request!(auth, "POST", "/ovarit_auth/create", {
                "username": user.username,
                "password": user.password,
                "email": user.email,
                "invite_code": code,
            });

            assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        }
    }
}

// Duplicate invite codes can be used.
#[tokio::test]
async fn test_register_with_duplicate_invite_code() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let user2 = User::random();
    let user3 = User::random();

    if cfg!(feature = "integration") {
        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
                INSERT INTO invite_code (uid, code, uses, max_uses, created)
                VALUES ('admin', 'two-use', 0, 1, now()),
                       ('admin', 'two-use', 0, 1, now())
            "#,
        )
        .execute(&pool)
        .await
        .unwrap();

        // use up both codes
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": user.username,
            "password": user.password,
            "email": user.email,
            "invite_code": "two-use",
        });
        assert_eq!(response.status(), StatusCode::CREATED);
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": user2.username,
            "password": user2.password,
            "email": user2.email,
            "invite_code": "two-use",
        });
        assert_eq!(response.status(), StatusCode::CREATED);

        // both should now be used up and unable to be used
        let response = make_request!(auth, "POST", "/ovarit_auth/create", {
            "username": user3.username,
            "password": user3.password,
            "email": user3.email,
            "invite_code": "two-use",
        });
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }
}

// Whitespace gets trimmed before using field values.
#[tokio::test]
async fn test_register_with_whitespace() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();
    let name = format!(" {} ", user.username);
    let email = format!(" {} ", user.email);

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": name,
        "password": user.password,
        "email": email,
        "invite_code": " default_code ",
    });
    assert_eq!(response.status(), StatusCode::CREATED);

    // The email should be sent to the address without the whitespace.
    let msgs = get_email_msgs!(&user.email);
    let Email { code, uid, .. } = &msgs[msgs.len() - 1];
    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::OK);

    // Ensure that the name without the whitespace can be used to log in.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    assert!(response.headers().get(SET_COOKIE).is_some());
}

// A user can delete their account.
#[tokio::test]
async fn test_retire_account() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    // Register, login and get a sesion cookie.
    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);
    let session = get_cookie_value(response.headers(), "session");

    // Try the retire request without the correct password.
    let response = make_request_with_session!(
    auth,
    "POST",
        "/ovarit_auth/retire",
    session,
    {
        "password": "not_the_password",
    });
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // The invalid request didn't do anything.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    assert_eq!(response.status(), StatusCode::OK);

    // Request the change, using the correct password.
    let response = make_request_with_session!(
    auth,
    "POST",
        "/ovarit_auth/retire",
    session,
    {
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // The old session cookie is now invalid.
    let response = make_request_with_session!(auth, "GET", "/ovarit_auth/info", session, {});
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // The user can no longer log in.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::NOT_FOUND);

    // The user can no longer reset their password.
    let email_count = get_email_msgs!(&user.email).len();
    let response = make_request!(auth, "POST", "/ovarit_auth/reset", {
        "username": user.username,
        "email": user.email,
    });
    // The reset request always says OK, but no email was sent.
    assert_eq!(response.status(), StatusCode::OK);
    assert_eq!(email_count, get_email_msgs!(&user.email).len());
}
