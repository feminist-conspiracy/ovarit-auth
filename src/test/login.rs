use super::*;

// The password is checked.
#[tokio::test]
async fn test_login_checks_password() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": "not your password",
    });
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
    assert!(response.headers().get(SET_COOKIE).is_none());
}

// Banned users cannot log in.
#[tokio::test]
async fn test_banned_user_login_rejection() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    if cfg!(feature = "integration") {
        register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

        let pool = create_pool().unwrap();
        sqlx::query(
            r#"
            UPDATE public.user
               SET status = 5, resets = resets + 1
             WHERE name = $1
            "#,
        )
        .bind(user.username.clone())
        .execute(&pool)
        .await
        .unwrap();

        let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
            "username": user.username,
            "password": user.password,
        });
        assert_eq!(response.status(), StatusCode::FORBIDDEN);
        assert!(response.headers().get(SET_COOKIE).is_none());
    }
}

// Login serves a remember cookie on request.
#[tokio::test]
async fn test_remember_cookie() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
        "remember_me": true,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);

    let session = get_cookie_value(response.headers(), "session");
    let uid1 = decode_and_check(session.as_bytes(), 60 * 60 * 24 * 30)
        .unwrap()
        ._user_id;

    let new_password = User::random_password();
    let response = make_request_with_session!(
    auth,
    "POST",
    "/ovarit_auth/update",
    session,
    {
        "password": user.password,
        "new_password": new_password,
        "new_email": null,
    });
    assert_eq!(response.status(), StatusCode::OK);

    // We now need to log in again.
    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": new_password,
        "remember_me": true,
    });
    assert_eq!(response.status(), StatusCode::ACCEPTED);

    let rem = get_cookie_value(response.headers(), "remember_token");
    let (uid_with_resets, _sig) = rem.split_once('|').unwrap();
    let (uid2, _resets) = uid_with_resets.split_once('$').unwrap();
    assert_eq!(uid1, uid2);
}
