use super::*;

// The verification link cannot be used twice.
#[tokio::test]
async fn test_verified_users_cannot_reverify() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;

    let msgs = get_email_msgs!(&user.email);
    assert_eq!(msgs.len(), 1);
    let Email { code, uid, .. } = &msgs[0];

    let response = make_request!(
        auth,
        "POST",
        "/ovarit_auth/create/validate",
        {
            "code": code,
            "uid": uid,
        }
    );
    assert_eq!(response.status(), StatusCode::NOT_FOUND);
    assert!(response.headers().get(SET_COOKIE).is_none());
}

// A registered and verified user cannot resend the registration link.
#[tokio::test]
async fn test_verified_user_cannot_resend_verification_link() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    register_and_verify(&mut auth, &user.username, &user.password, &user.email).await;
    let response = make_request!(auth, "POST", "/ovarit_auth/create/resend",
                                 {"username": user.username});
    // TODO: What should we do here? They're already verified but may
    // be having trouble logging in.
    assert_eq!(response.status(), StatusCode::OK);

    // Check that no new email was sent.
    let msgs = get_email_msgs!(&user.email);
    assert_eq!(msgs.len(), 1);
}

// A registered but unverified user can resend the registration link.
#[tokio::test]
async fn test_unverified_user_can_resend_verification_link() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::CREATED);

    let response = make_request!(auth, "POST", "/ovarit_auth/create/resend",
                                 {"username": user.username});
    assert_eq!(response.status(), StatusCode::OK);

    let msgs = get_email_msgs!(&user.email);
    assert_eq!(msgs.len(), 2);
    assert_eq!(msgs[0].ty, ContactType::Verify);
    assert_eq!(msgs[1].ty, ContactType::Verify);
}

// Registered users with unverified emails can't log in.
#[tokio::test]
async fn test_register_without_email_verification_fails_to_login() {
    let app_state = get_app_state().await;
    let mut auth = app(app_state);
    let user = User::random();

    let response = make_request!(auth, "POST", "/ovarit_auth/create", {
        "username": user.username,
        "password": user.password,
        "email": user.email,
        "invite_code": "default_code",
    });
    assert_eq!(response.status(), StatusCode::CREATED);

    let response = make_request!(auth, "POST", "/ovarit_auth/authenticate", {
        "username": user.username,
        "password": user.password,
    });
    assert_eq!(response.status(), StatusCode::FORBIDDEN);
}
